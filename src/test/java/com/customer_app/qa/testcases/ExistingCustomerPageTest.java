package com.customer_app.qa.testcases;
import static org.testng.Assert.assertFalse;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.ExistingCustomerPage;


import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({CustomerAllureListeners.class})
@Feature("Existing Customer")
@Epic("Regression Tests")
public class ExistingCustomerPageTest  extends TestBase {
	
	ExistingCustomerPage existingCustomerPage;
    
	
	public ExistingCustomerPageTest() throws IOException {
		super();

	}
	
	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();
		
		existingCustomerPage=new ExistingCustomerPage();
		
        //Click on Existing Customer Button
		existingCustomerPage.existingCustomer();
		
		
 }

	@Test(priority=0,description="Verifying Existing Customer Login with valid mobile Number")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying Existing Customer Login with valid mobile Number")
	@Story("Story Name : To check and Verify Existing Customer Login with valid mobile Number")	
	public void existingCustomer1() throws Exception {
		

		//Enter Valid Mobile Number
		existingCustomerPage.enterMobileNumber("8800735367");
		
		Thread.sleep(4000);

		
	}
	
	
	@Test(priority=1,description="Verifying that click on First existing customer and again click on back Icon")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying that click on First existing customer and again click on back Icon")
	@Story("Story Name : To check and Verify that click on First existing customer and again click on back Icon")	
	public void existingCustomer2() throws Exception {
		
		
		existingCustomerPage.navigateBack();
		
		
		Thread.sleep(5000);

		
	}
	

	@Test(priority=2,description="Verifying Existing Customer Login with valid mobile Number and again click on back icon")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying Existing Customer Login with valid mobile Number and again click on back icon")
	@Story("Story Name : To check and Verify Existing Customer Login with valid mobile Number and again click on back icon")	
	public void existingCustomer3() throws Exception {
		

		//Enter Valid Mobile Number
		existingCustomerPage.enterMobileNumber("8800735367");
		
		existingCustomerPage.navigateBack();
		
		
		Thread.sleep(5000);

		
	}

	@Test(priority=3,description="Verifying Existing Customer Login with valid mobile Number and valid Otp Number")
	@Severity(SeverityLevel.NORMAL)
	@Description("Test Case Description: Verifying Existing Customer Login with valid mobile Number and valid Otp Number")
	@Story("Story Name : To check and Verify Existing Customer Login with valid mobile Number and valid Otp Number")
	public void existingCustomer4() throws Exception {
		
		//Enter Valid Mobile Number and Otp
		existingCustomerPage.existingCustomerDetails("8800735367", "12468");
		
		Thread.sleep(5000);
		
	}
	
	
	
	@Test(priority=4,description="Verifying Existing Customer Login and also Verifying LogOut")
	@Severity(SeverityLevel.NORMAL)
	@Description("Test Case Description: Verifying Existing Customer Login and also Verifying LogOut")
	@Story("Story Name : To check and Verify Existing Customer Login and also Verifying LogOut")
	public void existingCustomer5() throws Exception {
		
		
		
		//Enter Valid Mobile Number and Otp
		existingCustomerPage.existingCustomerDetails("8800735367", "12468");
		
		
		//Click on Drawer Icon
		existingCustomerPage.moreLink();
		
		
		//Click on Logout Button
		existingCustomerPage.logoutText();
		
		
		//Click on Yes Button
		existingCustomerPage.yesPopupButton();
		
	    Thread.sleep(40000);
		

		
	}
	

	@Test(priority=5,description="Verifying Existing Customer Login with invalid mobile Number")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying Existing Customer Login with invalid mobile Number")
	@Story("Story Name : To check and Verify Existing Customer Login with invalid mobile Number")	
	public void existingCustomer6() throws Exception {
		

		//Enter invalid mobile Number
		existingCustomerPage.enterMobileNumber("880073536");

		assertFalse(true, "Enter a valid Phone Mobile Number");
		
	}
	
	@Test(priority=6,description="Verifying Existing Customer Login with valid mobile number but mobile number is new")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying Existing Customer Login with valid mobile number but mobile number is new")
	@Story("Story Name : To check and Verify Existing Customer Login with valid mobile number but mobile number is new")	
	public void existingCustomer7() throws Exception {
		

		//Enter invalid mobile Number
		existingCustomerPage.enterMobileNumber("8800735377");

		assertFalse(true, "OOPS!, This number is not registered with us. Please signup as anew user");
		
	}
	
	
	@Test(priority=7,description="Verifying Existing Customer Login with valid mobile Number and invalid Otp Number")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying Existing Customer Login with valid mobile Number and invalid Otp Number")
	@Story("Story Name : To check and Verify Existing Customer Login with valid mobile Number and invalid Otp Number")
	public void existingCustomer8() throws Exception {
		
		//Enter Valid Mobile Number and invalid Otp
		existingCustomerPage.existingCustomerDetails("8800735367", "");
		
		assertFalse(true, "Enter Otp Number");
		
	
	}
	
	

	@Test(priority=8,description="Verifying Existing Customer Login with valid mobile Number and again re-enter valid Otp Number")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying Existing Customer Login with valid mobile Number and  and again re-enter valid Otp Number")
	@Story("Story Name : To check and Verify Existing Customer Login with valid mobile Number and  and again re-enter valid Otp Number")
	public void existingCustomer9() throws Exception {
		
		existingCustomerPage.existingCustomerResend("8800735367", "12468","12468");
	
		
	}


	
	@AfterMethod
	public void End() {
	if(driver !=null) {
	driver.quit();
	
	}
	
	 
	}
	
}