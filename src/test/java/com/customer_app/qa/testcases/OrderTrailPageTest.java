package com.customer_app.qa.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;

import com.customer_app.qa.pages.CalenderPage;
import com.customer_app.qa.pages.ExistingCustomerPage;
import com.customer_app.qa.pages.OrderTrailPage;
import com.customer_app.qa.pages.ProfilePage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ CustomerAllureListeners.class})
@Feature("Existing Customer")
@Epic("Regression Tests")
public class OrderTrailPageTest extends TestBase {

	ExistingCustomerPage existingCustomerPage;

	OrderTrailPage orderTrailPage;

	ProfilePage profilePage;
	

	
	CalenderPage calenderPage;

	public OrderTrailPageTest() throws IOException {
		super();

	}

	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();

		existingCustomerPage = new ExistingCustomerPage();

		orderTrailPage = new OrderTrailPage();

		profilePage = new ProfilePage();
		

		
		calenderPage = new CalenderPage();
		
		
		

	}

	@Test(enabled=false,priority = 0, description = "Verifying placeorder with order trail with First updating profile details")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying placeorder with order trail with first updating profile details")
	@Story("Story Name : To check and Verify placeorder with order trail with first updating profile details")
	public void ordertrail() throws Exception {

	//	profilePage.moreLink();

		profilePage.profileLink();

//		profilePage.profileDetail("suraj mahato", "suraj@gmail.com", "9876543212", "room-202", "jmd megapolis",
//				"near by main road");

		profilePage.skipPopup();

		profilePage.navigateBack();

		profilePage.calenderText();

		orderTrailPage.ordertrailTap();

		orderTrailPage.updateBuffaloMilk();

		orderTrailPage.startDateButton();

		orderTrailPage.proceedButton();
		
        Thread.sleep(2000);
		
		orderTrailPage.proceedButton();

		orderTrailPage.payLaterButton();

		Thread.sleep(4000);

	}

	@Test(enabled = false, priority = 0, description = "Verifying placeorder with order trail without updating profile details first")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying placeorder with order trail without updating profile details first")
	@Story("Story Name : To check and Verify Verifying placeorder with order trail without updating profile details first")
	public void ordertrail1() throws Exception {

		orderTrailPage.ordertrailTap();

		orderTrailPage.updateBuffaloMilk();

		orderTrailPage.startDateButton();

		orderTrailPage.deliveryAddress();

		orderTrailPage.addFlatNo();

		orderTrailPage.customerDetails("suraj", "room-2020");

		orderTrailPage.saveAddress();

		orderTrailPage.proceedButton();

		orderTrailPage.payLaterButton();

	}
	
	@Test(enabled=false,priority = 0, description = "Verifying placeorder with order trail without updating profile details first and changing the area and city")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying placeorder with order trail without updating profile details first and changing the area and city")
	@Story("Story Name : To check and Verify Verifying placeorder with order trail without updating profile details first and changing the area and city")
	public void ordertrail3() throws Exception {

		orderTrailPage.ordertrailTap();

		orderTrailPage.updateMutipleProducts();

		orderTrailPage.startDateButton();

		orderTrailPage.deliveryAddress();

		orderTrailPage.addFlatNo();

		orderTrailPage.customerDetails("suraj", "room-2020");
		
		orderTrailPage.areaCitySection();
		
	//	newCustomerPage.selectCityLocalityBangalore();
	
		orderTrailPage.saveAddress();

		orderTrailPage.proceedButton();

		orderTrailPage.payLaterButton();
		
		Thread.sleep(4000);

	}
	
	

	@Test(priority = 0, description = "Verifying placeorder with order trail with updated profile details and changing the area and city")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying placeorder with order trail with updated profile details and changing the area and city")
	@Story("Story Name : To check and Verify placeorder with order trail with updated profile details and changing the area and city")
	public void ordertrail2() throws Exception {

		orderTrailPage.ordertrailTap();

		orderTrailPage.updateMutipleProducts();

		orderTrailPage.startDateButton();
		
		orderTrailPage.editLink();
		
		Thread.sleep(2000);
		
	    orderTrailPage.areaCitySection();
		
	//	newCustomerPage.selectCityLocalityBangalore();
		
		profilePage.doneButton();
		
		profilePage.skipPopup();
		
		orderTrailPage.proceedButton();
		
		Thread.sleep(2000);
		
		orderTrailPage.proceedButton();
				
        orderTrailPage.payLaterButton();
        
        Thread.sleep(3000);
        
        orderTrailPage.clickOnLeftArrowIcon();
        
        Thread.sleep(3000);
        
        calenderPage.maincalenderEndMonth("Jul, 2020");
        	
		Thread.sleep(4000);
		
		

	}
	
	
	
	
	
	

}