package com.customer_app.qa.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.InAppSupportPage;
import com.customer_app.qa.pages.VacationsPage;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ CustomerAllureListeners.class })
@Feature("In App Support")
@Epic("End To End Testing")
public class InAppSupportPageTest extends TestBase {

	InAppSupportPage inAppSupportPage;
	

	public InAppSupportPageTest() throws IOException {
		super();

	}
	

	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();

		inAppSupportPage = new InAppSupportPage();

	}

	/**
	 * Scenario is to validate clicking on Add Vacation Button.
	 */

	@Test(enabled=false,priority = 0, description = "to validate that by click one by one All Topics")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: To validate that by click one by one All Topics")
	@Story("Story Name : click one by one All Topics")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByClickingOneByOneAllTopicFiled() throws Exception {
	
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
			
		//click on one by one all topics
		inAppSupportPage.clickOneByOneAllTopics();

		Thread.sleep(4000);

	}
	
	@Test(enabled=false,priority = 1, description = "to validate that user order has delivered and Delivery Issue with order is not delivered") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that user order has delivered and Delivery Issue with order is not delivered")
	@Story("Story Name : user order has delivered and  Delivery Issue with order is not delivered")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithOrderIsNotDelivered() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("1");
		
		//Select the product divison
		inAppSupportPage.ValidatePleaseSelectProductDivison("1");
		
		//Register the complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");	
		
		//Are you want to exist
		inAppSupportPage.AreYouSureWantToExist("1");
		

		Thread.sleep(4000);

	}
	
	@Test(enabled=false,priority = 2, description = "to validate that user is in vacation and Delivery Issue with order is not delivered ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate  that user is in vacation and Delivery Issue with order is not delivered")
	@Story("Story Name : user is in vacation and Delivery Issue with order is not delivered")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithOrderIsNotDeliveredButVacationCase() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("2");
		
		//Register the complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");	
		
		//Are you want to exist
		inAppSupportPage.AreYouSureWantToExist("1");
		

		Thread.sleep(4000);

	}
	
	
	@Test(enabled=false,priority = 3, description = "to validate that user order on Hold and Delivery Issue with order is not delivered ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate  that user order on hold and Delivery Issue with order is not delivered")
	@Story("Story Name : User order on hold and Delivery Issue with order is not delivered")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithOrderIsNotDeliveredButOrderHoldCase() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("3");
		
		//Register the complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");	
		
		//Are you want to exist
		inAppSupportPage.AreYouSureWantToExist("1");
		

		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 4, description = "to validate that Route sheet not genereated and Delivery Issue with order is not delivered ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate Route sheet not genereated and Delivery Issue with order is not delivered")
	@Story("Story Name : Route sheet not genereated and Delivery Issue with order is not delivered")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithOrderIsNotDeliveredButRouteSheetNotgenereatedCase() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("5");
		
		//Register the complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");	
		
		//Are you want to exist
		inAppSupportPage.AreYouSureWantToExist("1");
		

		Thread.sleep(4000);

	}
	
	@Test(enabled=false,priority = 5, description = "to validate that Route sheet genereated and Delivery Issue with order is not delivered ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate Route sheet genereated and Delivery Issue with order is not delivered")
	@Story("Story Name : Route sheet genereated and Delivery Issue with order is not delivered")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithOrderIsNotDeliveredButRouteSheetgenereatedCase() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("6");
		
		//Register the complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");	
		
		//Are you want to exist
		inAppSupportPage.AreYouSureWantToExist("1");
		

		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 6, description = "to validate that Delivery Issue with product missing of Dairy product ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Delivery Issue with product missing of Dairy product")
	@Story("Story Name :  Delivery Issue with product missing of Dairy product")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithProductMissingofDairyProduct() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("2");
		
		//select the date
		inAppSupportPage.selectTheDate("6");
		
        //select the iteam
		inAppSupportPage.PleaseChoosetheProductsFromBelow("3");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//update the qty
		inAppSupportPage.PleaseUpdatetheQty("1");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		

		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 7, description = "to validate that Delivery Issue with Wrong product of FNV product ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Delivery Issue with Wrong product of FNV product")
	@Story("Story Name :  Delivery Issue with Wrong product of FNV product")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateDeliveryIssueWithWrongProductofFNVProduct() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("3");
		
		//select the date
		inAppSupportPage.selectTheDate("7");
		
        //select the iteam
		inAppSupportPage.PleaseChoosetheProductsFromBelow("3");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//update the qty
		inAppSupportPage.PleaseUpdatetheQty("1");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		

		Thread.sleep(4000);

	}
	


	@Test(enabled=false,priority = 8, description = "to validate that already complaints rasie and again click on Delivery Issue with Wrong product of FNV product ") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that already complaints rasie and again click on Delivery Issue with Wrong product of FNV product")
	@Story("Story Name :  already complaints rasie and again click on Delivery Issue with Wrong product of FNV product")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateAlreadyDeliveryIssueWithWrongProductofFNVProduct() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("3");
		
		//select the date
		inAppSupportPage.selectTheDate("7");
		
        //select the iteam
		inAppSupportPage.PleaseChoosetheProductsFromBelow("2");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		

		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 9, description = "to validate that By select one by one all options of other delivery requests") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that By select one by one all options of other delivery requests")
	@Story("Story Name :  By select one by one all options of other delivery requests")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectAllOPtionsOfOtherDeliveryRequest() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("4");
		
		//By clicking one by one option other delivery requests
		inAppSupportPage.clickOneByOneOptionOfOtherDeliveryRequest();

		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 10, description = "to validate that Select Delivery issue and then select Previous menu") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Delivery issue and then select Previous menu")
	@Story("Story Name :  Select Delivery issue and then select Previous menu")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectDeliveryIssuewithPreviousMenu() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue and order is is not delivered filed
		inAppSupportPage.ValidateBySelectanyOneOptionFromDeliveryIssue("5");

		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 11, description = "to validate that Select Billing issue and also select product was not delivered but charged and finally select Yes button") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Billing issue and also select product was not delivered but charged and finally select Yes button")
	@Story("Story Name :  Select Billing issue and also select product was not delivered but charged and finally select Yes button")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithProductwasNotDelivered() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("1");
		
		//Select the product divison
		inAppSupportPage.ValidatePleaseSelectProductDivison("2");
		
		//select Yes button
		inAppSupportPage.ValidatePleaseConfirmtheDelivery("1");
		

		Thread.sleep(4000);

	}
	


	@Test(enabled=false,priority = 12, description = "to validate that Select Billing issue and also select product was not delivered but charged and finally select Not Received button") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Billing issue and also select product was not delivered but charged and finally select Not Received button")
	@Story("Story Name :  Select Billing issue and also select product was not delivered but charged and finally select Not Received  button")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithProductwasNotDeliveredNotRecevied() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("1");
		
		//select the date
		inAppSupportPage.selectTheDate("1");
		
		//Select the product divison
		inAppSupportPage.ValidatePleaseSelectProductDivison("1");
		
		//select Not Received button
		inAppSupportPage.ValidatePleaseConfirmtheDelivery("2");
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		Thread.sleep(4000);

	}
	


	@Test(enabled=false,priority = 13, description = "to validate that Select Billing issue and also select discount related issue and finally select the product i.e product have no offer applied") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that  Select Billing issue and also select discount related issue and finally select the product i.e product have no offer applied")
	@Story("Story Name :   Select Billing issue and also select discount related issue and finally select the product i.e product have no offer applied")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithDiscountRelatedIssue() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("3");
		
		//select the date
		inAppSupportPage.selectTheDate("1");
		
		//Select the product divison
		inAppSupportPage.ValidatePleaseSelectProductDivison("1");
		
		//select the product
		inAppSupportPage.ValidatePleaseSelectTheproductForwhichyoudidnotreceivediscount("1");
			
		//Do you want to register a complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		
		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 14, description = "to validate that Select Billing issue and also select discount related issue and finally select the product i.e product have offer applied") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that  Select Billing issue and also select discount related issue and finally select the product i.e product have offer applied")
	@Story("Story Name : Select Billing issue and also select discount related issue and finally select the product i.e product have offer applied")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithDiscountRelatedIssuewithOffer() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("3");
		
		//select the date
		inAppSupportPage.selectTheDate("1");
		
		//Select the product divison
		inAppSupportPage.ValidatePleaseSelectProductDivison("1");
		
		//select the product
		inAppSupportPage.ValidatePleaseSelectTheproductForwhichyoudidnotreceivediscount("2");
			
		//Do you want to register a complaints
		inAppSupportPage.ValidateDoYouWantToRegisterAComplaints("1");
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		
		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 15, description = "to validate that Select Billing issue and also select Cashback related issues and select first recharge cashback") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Billing issue and also select Cashback related issues and select first recharge cashback")
	@Story("Story Name : Select Billing issue and also select Cashback related issues and select first recharge cashback")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithCashbackRelatedIssuewithFirstRechargeCashback() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("4");
		
        //select the first recharge cashback
		inAppSupportPage.ValidatePleaseSelectCategoryOfCashbackRelatedIssues("1");
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		
		Thread.sleep(4000);

	}

	

	@Test(enabled=false,priority = 16, description = "to validate that Select Billing issue and also select Cashback related issues and select referral cashback") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Billing issue and also select Cashback related issues and select referral cashback")
	@Story("Story Name : Select Billing issue and also select Cashback related issues and select referral cashback")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithCashbackRelatedIssuewithReferralCashback() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("4");
		
        //select the first recharge cashback
		inAppSupportPage.ValidatePleaseSelectCategoryOfCashbackRelatedIssues("2");
		
		//Are you satisfied
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		
		Thread.sleep(4000);

	}
	

	@Test(enabled=false,priority = 17, description = "to validate that Select Billing issue and also select Cashback related issues and previous menu") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Billing issue and also select Cashback related issues and select previous menu")
	@Story("Story Name : Select Billing issue and also select Cashback related issues and select previous menu")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateBYSelectBillingIssuewithCashbackRelatedIssuewithReferralPreviousMenu() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on billing issue
		inAppSupportPage.ValidateBySelectanyOneOptionFromBillingPaymentIssue("4");
		
        //select the first recharge cashback
		inAppSupportPage.ValidatePleaseSelectCategoryOfCashbackRelatedIssues("3");
		
		
		Thread.sleep(4000);

	}
	


	@Test(enabled=false,priority = 18, description = "to validate that Select profile related requests and also select update mobile number and enter existing mobile no and select as yes") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select profile related requests and also select update mobile number and enter existing mobile no select as yes ")
	@Story("Story Name : Select profile related requests and also select update mobile number and enter existing mobile no select as yes ")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByProfileRelatedRequestsUpdateMobileNo() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select profile related requests
		inAppSupportPage.ValidateBySelectanyOneOptionFromProfileRelatedRequests("1");

		//enter new number
		inAppSupportPage.enterNewNumber("9515321546");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//click on Yes button
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		Thread.sleep(4000);

	}
	


	@Test(enabled=false,priority = 19, description = "to validate that Select profile related requests and also select update mobile number and enter existing mobile no and select as No") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select profile related requests and also select update mobile number and enter existing mobile no select as No ")
	@Story("Story Name : Select profile related requests and also select update mobile number and enter existing mobile no select as No ")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByProfileRelatedRequestsUpdateMobileNoandSelectNo() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select profile related requests
		inAppSupportPage.ValidateBySelectanyOneOptionFromProfileRelatedRequests("1");

		//enter new number
		inAppSupportPage.enterNewNumber("9515321546");
		
		//click on submit button
		inAppSupportPage.clickonSUbmitButton();
		
		//click on Yes button
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("2");
		
		//click on Yes button
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		Thread.sleep(4000);

	}
	


	@Test(enabled=false,priority = 20, description = "to validate that Select profile related requests and also select Address update and select as Yes") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select profile related requests and also select Address update and select as Yes")
	@Story("Story Name : Select profile related requests and also select Address update and select as Yes ")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByProfileRelatedRequestsAddressupdate() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select profile related requests
		inAppSupportPage.ValidateBySelectanyOneOptionFromProfileRelatedRequests("2");

		//click on add flat no button
		inAppSupportPage.clickonAddFlatnOButton();
		
		//update the address
		inAppSupportPage.UpdateFlatNoandDeliveryLocation("flatno-600 6th floor", "sector-45 ");
		
		//click on save button
		inAppSupportPage.clickonSaveAddressButton();
		
		//click on Yes button
		inAppSupportPage.ValidateAreYouSatisfiledWithOurSupport("1");
		
		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		Thread.sleep(2000);
		
		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		//click on my profile
		inAppSupportPage.clickonMyProfileFiled();
		
		Thread.sleep(4000);

	}
	
	

	@Test(enabled=false,priority = 21, description = "to validate that Select profile related requests and also select stop all the services and select one by one option from list") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select profile related requests and also select stop all the services and select one by one option from list")
	@Story("Story Name : Select profile related requests and also select stop all the services and select one by one option from list ")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByStopsAllTheServicesOneByOne() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select profile related requests
		inAppSupportPage.ValidateBySelectanyOneOptionFromProfileRelatedRequests("3");
		
		//select one by one option of stops all the services
		inAppSupportPage.clickOneByOneOptionOfStopsAlltheServices();
		
		Thread.sleep(4000);
		
		
		
	}
	

	@Test(enabled=false,priority = 22, description = "to validate that Select Call Us Filed") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Call Us Filed")
	@Story("Story Name : Select Call Us Filed ")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByClickOnCallUsFiled() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select call us filed
		inAppSupportPage.clickOnCallUsFiled();

		
		Thread.sleep(4000);
		
		
		
	}
	
	

	@Test(enabled=false,priority = 23, description = "to validate that Select Call Us Filed and click on call us button") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Call Us Filed and click on call us button")
	@Story("Story Name : Select Call Us Filed and click on call us button")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByClickOnCallUsFiledandClickOnCallUSButton() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select call us filed
		inAppSupportPage.clickOnCallUsFiled();
		
		//click on call us button
		inAppSupportPage.clickOnCallUsButton();		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		
		Thread.sleep(4000);
		
		
		
	}
	

	@Test(enabled=false,priority = 24, description = "to validate that Select Call Us Filed and click on call us button") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that Select Call Us Filed and click on call us button")
	@Story("Story Name : Select Call Us Filed and click on call us button")
	@Link("Customer App version 3.5.3.apk")
	public void ToValidateByClickOnEmailUsLists() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
        //select email us filed
		inAppSupportPage.clickOnEmailUsFiled();
		
		//select one by one write to us dropdwon list
		inAppSupportPage.clickOneByOneOptionOfWriteToUsFiled();
		
		
		Thread.sleep(4000);
		
		
		
	}
	


	@Test(priority = 25, description = "to validate that open consersation of delivery issue") 
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: to validate that open consersation of delivery issue")
	@Story("Story Name : open consersation of delivery issue")
	@Link("Customer App version 3.5.3.apk")
	public void ToVerifyOpenChatScreen() throws Exception {

		
		//click on menu icon
		inAppSupportPage.clickOnMenuIcon();
		
		//click on support filed
		inAppSupportPage.clickOnSupportFiled();
		
		//click on delivery issue
		inAppSupportPage.clickonDeliveryIssue();
			
		//Are you want to exist
		inAppSupportPage.AreYouSureWantToExist("1");
		
		//click on open conversation filed
		inAppSupportPage.clickOnanyOpenConversationFiled("1");    
		
		Thread.sleep(4000);
		
		
		
	}
	

	
	
	

	@AfterMethod
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}

}
