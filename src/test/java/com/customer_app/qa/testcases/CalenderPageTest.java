package com.customer_app.qa.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.CalenderPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ CustomerAllureListeners.class })
@Feature("Existing Customer")
@Epic("Regression Tests")
public class CalenderPageTest extends TestBase {
	
	CalenderPage calenderPage;

	public CalenderPageTest() throws IOException {
		super();

	}

	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();
		
		calenderPage = new CalenderPage();
		
		
	}

	@Test(priority = 0, description = "Verifying placeorder with order trail with First updating profile details")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying placeorder with order trail with first updating profile details")
	@Story("Story Name : To check and Verify placeorder with order trail with first updating profile details")
	public void ordertrail() throws Exception {

	/*	
		calenderPage.maincalenderEndMonth("Jul, 2020");
		
		Thread.sleep(2000);
		
		calenderPage.maincalenderStartMonth("Apr, 2020");
			
		Thread.sleep(4000);

 */
		
		
		calenderPage.maincalenderEndMonthSwipe("Jul, 2020");
		
		Thread.sleep(2000);
		
		calenderPage.maincalenderStartMonthSwipe("Apr, 2020");
		
		Thread.sleep(4000);
		
	}
	
}
		
		
