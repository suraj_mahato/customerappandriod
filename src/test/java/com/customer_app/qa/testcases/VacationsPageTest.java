package com.customer_app.qa.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.CalenderPage;
import com.customer_app.qa.pages.MyPlanPage;
import com.customer_app.qa.pages.ProfilePage;
import com.customer_app.qa.pages.VacationsPage;
import com.customer_app.qa.utilities.CalenderUtility;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ CustomerAllureListeners.class })
@Feature("Vacations")
@Epic("Regression Tests")
public class VacationsPageTest extends TestBase {
	
	VacationsPage vacationPage;
	

	
	public VacationsPageTest() throws IOException {
		super();

	}

	
	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();
		
		vacationPage = new VacationsPage();
	

	}

	
	/**
	 * Scenario is to validate clicking on Add Vacation Button.
    */

	@Test(priority = 0, description = "to validate clicking on Add Vacation Button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and clicking on Add Vacation Button")
	@Story("Story Name : clicking on Add Vacation Button")
	public void addVacationsButton() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		
		//click on ADD Vacation button
		vacationPage.clickOnAddVacationButton();
		
		Thread.sleep(2000);
		
		
		//click on left arrow iocn
		vacationPage.clickOnLeftArrowIconButton();
		
		Thread.sleep(4000);
		
	
		

	}
	

	/**
	 * Scenario is to validate clicking on plus icon Button.
    */

	@Test(enabled=false, priority = 1, description = "to validate clicking on plus icon Button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and clicking on plus icon button")
	@Story("Story Name : clicking on plus icon Button")
	public void plusIconButton() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		//click on plus icon button
		vacationPage.clickOnPlusIconButton();
		
		Thread.sleep(2000);
		
		//click on left arrow icon
		vacationPage.clickOnLeftArrowIconButton();
		
		Thread.sleep(4000);
		
		

	}
	
	
	
	
	/**
	 * Scenario is to validate and add vacations with select start date only and keeping end date optional.
	 */

	@Test(enabled=false,priority = 2, description = "To validate and add vacations with select start date only and keeping end date optional ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and select Start Date and keep End Date option")
	@Story("Story Name : Adding Vacations with select start date and keeping End Date Optional")
	public void addingVacations() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		//click on ADD Vacation button
		vacationPage.clickOnAddVacationButton();
		
		Thread.sleep(3000);
		
		//click on ADD Vacation button
		vacationPage.clickOnAddVacationButton();
		
		Thread.sleep(3000);
		//click on left arrow icon
		vacationPage.clickOnLeftArrowIconButton();
		
		//click on HomeIcon
		vacationPage.clickOnHomeIcon();
		Thread.sleep(4000);
		
	

	}
	

	/**
	 * Scenario is to deleting running vacations..
	 */

	@Test(enabled=false,priority = 3, description = "To validate and deleting the vacations ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and deleting the vacations ")
	@Story("Story Name : deleting the vacations ")
	public void deletingVacations() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		//click on already running vacation section text
		vacationPage.clickOnalredayVacationSectionText();
		
		//click on end vacation link
		vacationPage.clickOnEndVacationLink();
		Thread.sleep(4000);

		//click on left arrow icon
		vacationPage.clickOnLeftArrowIconButton();
		
		//click on HomeIcon
		vacationPage.clickOnHomeIcon();
		Thread.sleep(4000);
		
	

	}
	
	
	
	/**
	 * Scenario is to validate and add vacations with select start date and end date.
	 */

	@Test(enabled=false,priority = 4, description = "To validate and add vacations with select start date and end date ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and select Start Date and End Date")
	@Story("Story Name : Adding Vacations with select start date and End Date ")
	public void addingVacations1() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		//click on ADD Vacation button
		vacationPage.clickOnAddVacationButton();
		
		//click on End Date section
		vacationPage.clickOnOptionalSection();
		
		//click on Ok Button
		vacationPage.clickOnOkButton();
		
		//click on particular date
		vacationPage.clickOnParticularDate();
		
		Thread.sleep(3000);
		
		//click on ADD Vacation button
		vacationPage.clickOnAddVacationButton();
		
		Thread.sleep(3000);
		//click on left arrow icon
		vacationPage.clickOnLeftArrowIconButton();
		
		//click on HomeIcon
		vacationPage.clickOnHomeIcon();
		Thread.sleep(4000);
		
	
	}
	
	/**
	 * Scenario is to deleting running vacations..
	 */

	@Test(enabled=false,priority = 5, description = "To validate and deleting the vacations ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and deleting the vacations ")
	@Story("Story Name : deleting the vacations ")
	public void deletingVacations1() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		//click on already running vacation section text
		vacationPage.clickOnalredayVacationSectionText();
		
		//click on end vacation link
		vacationPage.clickOnEndVacationLink();
		Thread.sleep(4000);

		//click on left arrow icon
		vacationPage.clickOnLeftArrowIconButton();
		
		//click on HomeIcon
		vacationPage.clickOnHomeIcon();
		Thread.sleep(4000);
			

	}

	@Test(enabled=false,priority = 5, description = "To validate and deleting the vacations ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to vacations section and deleting the vacations ")
	@Story("Story Name : deleting the vacations ")
	public void adding() throws Exception {

		Thread.sleep(3000);
		// click on menu text 
		vacationPage.clickOnMenuIcon();
		
		//click on vacation text
		vacationPage.clickOnVacationText();
		
		//click on ADD Vacation button
		vacationPage.clickOnAddVacationButton();
		
		//click on End Date section
		vacationPage.clickOnOptionalSection();
		
		//click on Ok Button
	
		vacationPage.clickOnOkButton();
		
		Thread.sleep(4000);
		
		vacationPage.clickOnparticulardateRandomly();
		Thread.sleep(4000);
			
	
	}
	
	@AfterMethod
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}

}
