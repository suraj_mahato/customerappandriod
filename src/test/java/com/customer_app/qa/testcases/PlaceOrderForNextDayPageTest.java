package com.customer_app.qa.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.PlaceOrderForNextDayPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;


@Listeners({ CustomerAllureListeners.class })
@Feature("Existing Customer")
@Epic("Regression Tests")
public class PlaceOrderForNextDayPageTest extends TestBase {
	
	PlaceOrderForNextDayPage placeanOrderForNextDayPage;

	public PlaceOrderForNextDayPageTest() throws IOException {
		super();

	}

	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();
		
		placeanOrderForNextDayPage = new PlaceOrderForNextDayPage();
		
		
	}

	@Test(enabled=false,priority = 0, description = "Verifying place an order For next day")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying place an order For next day")
	@Story("Story Name : To check and Verify place an order For next day")
	public void placeaonOrderForNextDay() throws Exception {

		Thread.sleep(2000);
		placeanOrderForNextDayPage.clickOnMenuIcon();
		
		placeanOrderForNextDayPage.clickOnSupportButton();
		
		placeanOrderForNextDayPage.clickOnGettingStartedText();
		
		placeanOrderForNextDayPage.clickOnPlaceanOrderSection();
		
		placeanOrderForNextDayPage.clickOnPlaceanOrderForNextDayButton();
		
		Thread.sleep(4000);
		
	}
	

	@Test(priority = 1, description = "Verifying place an order For next day")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verifying place an order For next day")
	@Story("Story Name : To check and Verify place an order For next day")
	public void productTitle() throws Exception {

		Thread.sleep(4000);
		placeanOrderForNextDayPage.clickOnMenuIcon();
		
		placeanOrderForNextDayPage.clickOnSupportButton();
		
		placeanOrderForNextDayPage.clickOnGettingStartedText();
		
		placeanOrderForNextDayPage.clickOnPlaceanOrderSection();
		
		placeanOrderForNextDayPage.clickOnPlaceanOrderForNextDayButton();
		
		placeanOrderForNextDayPage.titleOfProduct1();
		
		Thread.sleep(4000);
		
	}
	
	
	
	
}
