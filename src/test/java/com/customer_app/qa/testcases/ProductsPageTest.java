package com.customer_app.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.pages.ProductsPage;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class ProductsPageTest extends TestBase {

	ProductsPage productsPage;
	

	public ProductsPageTest() throws IOException {
		super();

	}

	
	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();

		productsPage = new ProductsPage();
		

	}
	
	
	@Test(priority = 0, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on my plan Banner icon on homepage ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to Banner section and click on my plan Banner icon on homepage")
	@Story("Story Name : click on my plan Banner icon on homepage")
	public void clickRandomlyHorizontalProductNameList() throws Exception {

		Thread.sleep(2000);
        productsPage.productsLink();
		
		String productsPages=productsPage.verifyProductsPage();
		
		Assert.assertEquals(productsPages, "Products", "Products page not displayed" );
		

	}
	
	@AfterMethod
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}
	
}