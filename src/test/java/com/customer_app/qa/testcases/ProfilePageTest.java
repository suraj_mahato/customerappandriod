package com.customer_app.qa.testcases;

import static org.testng.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.ExistingCustomerPage;
import com.customer_app.qa.pages.ProfilePage;
import com.customer_app.qa.utilities.ExcelReader;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ CustomerAllureListeners.class })
@Feature("Profile Screen")
@Epic("End To End Testing")
public class ProfilePageTest extends TestBase {

	ExistingCustomerPage existingCustomerPage;

	ProfilePage profilePage;

	String sheetName = "customerdetails";

	public ProfilePageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Andriod Devices Start Up")
	public void setUp() throws Exception {

		initiallization();

		log.debug("********INITIALIZING FROM TESTBASE*******");

		profilePage = new ProfilePage();

		existingCustomerPage = new ExistingCustomerPage();

	}

	@DataProvider
	public Object[][] cdTestData() {

		Object data[][] = ExcelReader.getTestData(sheetName);
		return data;

	}
	
	@Test(enabled=false,priority = 0, description = "To Validate that first move menu screen --> click on Wallet filed --> it's navigate to Address screen")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on Wallet filed --> it's navigate to Address screen")
	@Story("Story Name : To check and Verify first move menu screen --> click on Wallet filed --> it's navigate to Address screen")
	@Link("Customer App 5.2.1.apk")
	public void NewUserClickOnWalletFiled() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on wallet filed
		profilePage.ClickonWallet();
		
		Thread.sleep(5000);
		
		
	}
	
	@Test(enabled=false,priority = 1, description = "To Validate that first click on products link --> select the dairy product --> Select the cow milk product ---> Update the qty=1 --> click on confirm button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on products link --> select the dairy product --> Select the cow milk product ---> Update the qty=1 --> click on confirm button")
	@Story("Story Name : To check and Verify first click on products link --> select the dairy product --> Select the cow milk product ---> Update the qty=1 --> click on confirm button")
	@Link("Customer App 5.2.1.apk")
	public void NewUserClickOnProducts() throws Exception {
		
	    //Select the product and order place
		profilePage.ClickonProducts();
		
		Thread.sleep(5000);
		
		
	}
	
	@Test(enabled=false,priority = 2, description = "To Validate that first click on support link --> click on place an order for next day button --> Select the test milk product ---> Update the qty=1 --> click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first click on support link --> click on place an order for next day button --> Select the test milk product ---> Update the qty=1 --> click on update button")
	@Story("Story Name : To check and Verify first click on support link --> click on place an order for next day button --> Select the test milk product ---> Update the qty=1 --> click on update button")
	@Link("Customer App 5.2.1.apk")
	public void NewUserClickOnSupports() throws Exception {


		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();
		
	    //click on support first
		profilePage.ClickOnSupportFirstSelectTheProduct();
		
		Thread.sleep(5000);
		
		
	}
	
	@Test(enabled = false, priority = 3, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid in Name Filed, valid flatNo and valid Delivery location--> click on skip preference for now link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid in Name Filed, valid flatNo and valid Delivery location--> click on skip preference for now link")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid in Name Filed, valid flatNo and valid Delivery location--> click on skip preference for now link")
	@Link("Customer App 5.2.1.apk")
	public void NewUserCreatingProfileDetails() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.NewUserprofileDetail(prop.getProperty("name"), prop.getProperty("flatno"),
				prop.getProperty("dlocation"));
		
		//click on save address button
		profilePage.ClickOnSaveAddressButton();
		

		Thread.sleep(2000);
		//click on skip preference for now link
		profilePage.ClickOnSkipPreferenceForNowLink();
		

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}
	
	
	

	@Test(enabled = false, priority = 4, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Upper case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Upper case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Upper case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithValidName() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("ValidName"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 5, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Upper case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Upper case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Upper case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithPreviousSameValidName() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("ValidName"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 6, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Lower case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Lower case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid all Lower case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithAllLowerCaseValidName() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("LowerCaseTextValidName"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 7, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid Upper and Lower case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid upper and Lower case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid Upper and Lower case in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithUpperLowerCaseValidName() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("ULValidName"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 8, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid large text in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid large text in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Valid large text in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithLargeValidName() throws Exception {

		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("LargetextValidText"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 9, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter invalid numeric value in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter invalid numeric value in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter invalid numeric value in Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithInValidNameandValidEmailIdAltNo() throws Exception {

		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("InValidName"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 10, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon --> Enter Blank inValid Name Filed, valid email id and valid alternate no--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon --> Enter Blank inValid Name Filed, valid email id and valid alternate no--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon --> Enter Blank inValid Name Filed, valid email id and valid alternate no--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void ProfileDetailsWithInValidNameBlankandValidEmailIdAltNo() throws Exception {

		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// enter name, email id and alternate no
		profilePage.profileDetail(prop.getProperty("BlankName"), prop.getProperty("ValidEmailId"),
				prop.getProperty("ValidAletrnateNo"));

		// click on Done button
		profilePage.doneButton();

		Thread.sleep(3000);

		String ActualResult = "Validation message should be display";

		String ExpectedResult = "Validation message should be display";

		Assert.assertEquals(ActualResult, ExpectedResult, "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 11, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Valid flatNo Filed--> click on SaveAddress button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Valid flatNo Filed--> click on SaveAddress button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Valid flatNo Filed--> click on SaveAddress button")
	@Link("Customer App 5.2.1.apk")
	public void EnterFlatNoinAddressFiled() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on address edit icon
		profilePage.editAddresslink();

		// enter valid flat no
		profilePage.AdressDetails(prop.getProperty("ValidFlatNo"), prop.getProperty("ValidDlocation"));

		// click on save Address button
		profilePage.ClickOnSaveAddressButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 12, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Valid delivery location Filed--> click on SaveAddress button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Valid delivery location  Filed--> click on SaveAddress button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Valid delivery location  Filed--> click on SaveAddress button")
	@Link("Customer App 5.2.1.apk")
	public void EnterDeliveryLocationinAddressFiled() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on address edit icon
		profilePage.editAddresslink();

		// enter valid flat no
		profilePage.AdressDetails(prop.getProperty("ValidFlatNo"), prop.getProperty("ValidDlocations"));

		// click on save Address button
		profilePage.ClickOnSaveAddressButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 13, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Blank in flat no Filed--> click on SaveAddress button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Blank in flat no Filed--> click on SaveAddress button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Blank in flat no Filed--> click on SaveAddress button")
	@Link("Customer App 5.2.1.apk")
	public void EnterBlankInFlatNoFiledinAddressFiled() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on address edit icon
		profilePage.editAddresslink();

		// enter valid flat no
		profilePage.AdressDetails(prop.getProperty("BlankFlatNo"), prop.getProperty("ValidDlocations"));

		// click on save Address button
		profilePage.ClickOnSaveAddressButton();

		Thread.sleep(3000);

		String ActualResult = "Please fill flatno/House/Building";
		String ExpectedResult = "Please fill flatno/House/Building";

		Assert.assertEquals(ActualResult, ExpectedResult, "Validation message is not displaying");

	}

	@Test(enabled = false, priority = 14, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Blank in delivery locationFiled--> click on SaveAddress button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Blank in delivery location  Filed--> click on SaveAddress button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Enter Blank in delivery location  Filed--> click on SaveAddress button")
	@Link("Customer App 5.2.1.apk")
	public void EnterBlankDeliveryLocationinAddressFiled() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on address edit icon
		profilePage.editAddresslink();

		// enter valid flat no
		profilePage.AdressDetails(prop.getProperty("ValidFlatNo"), prop.getProperty("BlankDlocation"));

		// click on save Address button
		profilePage.ClickOnSaveAddressButton();

		Thread.sleep(3000);

		String ActualResult = "Please fill Society and Locality";
		String ExpectedResult = "Please fill Society and Locality";

		Assert.assertEquals(ActualResult, ExpectedResult, "Validation message is not displaying");

	}

	@Test(enabled = false, priority = 15, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Select city and locality--> click on SaveAddress button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Select city and locality--> click on SaveAddress button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of Address Section --> Select city and locality --> click on SaveAddress button")
	@Link("Customer App 5.2.1.apk")
	public void SelectingCityandLocality() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on address edit icon
		profilePage.editAddresslink();

		// enter valid flat no
		profilePage.AdressDetails(prop.getProperty("ValidFlatNo"), prop.getProperty("ValidDlocation"));

		// Select city and locality
		profilePage.validateselectinganyCityLocality();

		Thread.sleep(3000);

		// click on save Address button
		profilePage.ClickOnSaveAddressButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 16, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Don't Ring the bell--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Don't Ring the bell--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Don't Ring the bell--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void DonotRingtheBell() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Select Don't ring the bell
		profilePage.DonotSelectRingBellRadioButton();

		// click on done button
		profilePage.DeliveryPreferenceDoneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 17, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Ring the bell--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Ring the bell--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Ring the bell--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void RingtheBell() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Select ring the bell
		profilePage.SelectRingBellRadioButton();

		// click on done button
		profilePage.DeliveryPreferenceDoneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 18, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select evening timeslot--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select evening timeslot--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select evening timeslot--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void SelectEveningTimeslot() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Select evening timeslot
		profilePage.SelectEveningTimeSlotButton();

		// click on done button
		profilePage.DeliveryPreferenceDoneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 19, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Moring timeslot--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Moring timeslot--> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Select Moring timeslot--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void SelectMoringTimeslot() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Select Moring timeslot
		profilePage.SelectMoringTimeSlotButton();

		// click on done button
		profilePage.DeliveryPreferenceDoneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled = false, priority = 20, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Adding Voice--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Adding Voice --> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Adding Voice--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void VoiceaddingInstruction() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Adding Voice
		profilePage.AddingVoice();

		// click on done button
		profilePage.DeliveryPreferenceDoneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled=false, priority = 21, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Retry the Voice--> click on Done button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference -->Retry the Voice --> click on Done button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Retry the Voice--> click on Done button")
	@Link("Customer App 5.2.1.apk")
	public void RetryVoiceaddingInstruction() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Retry the Voice
		profilePage.RetryVoice();

		// click on done button
		profilePage.DeliveryPreferenceDoneButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	@Test(enabled=false,priority = 22, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Retry the Voice--> click on mobile os back button --> finally click on save button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference -->Retry the Voice --> click on mobile os back button --> finally click on save button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Retry the Voice--> click on mobile os back button --> finally click on save button")
	@Link("Customer App 5.2.1.apk")
	public void ClickOnMobileOSBackButton() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Retry the Voice
		profilePage.RetryVoice();

		// click on mobile os back button
		profilePage.ClickOnMobileOSBackButton();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}


	@Test(enabled=false,priority = 23, description = "To Validate that first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Retry the Voice--> click on back arrow icon --> finally click on save button")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: first move menu screen --> click on My profile filed --> click on edit icon of delivery preference -->Retry the Voice --> click on back arrow icon --> finally click on save button")
	@Story("Story Name : To check and Verify first move menu screen --> click on My profile filed --> click on edit icon of delivery preference --> Retry the Voice--> click on back arrow icon --> finally click on save button")
	@Link("Customer App 5.2.1.apk")
	public void ClickOnBackArrowIcon() throws Exception {

		Thread.sleep(3000);
		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// Retry the Voice
		profilePage.RetryVoice();

		// click on back icon
		profilePage.ClickOnBackIcon();

		Thread.sleep(3000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}
	
	
	@Test(enabled = false, dataProvider = "cdTestData", description = "Verifying customer Order with enter valid AND invalid Customer Details with Name, Mobile, Alt No, Email, Flat No and Locality")
	@Severity(SeverityLevel.NORMAL)
	@Description("Test Case Description: Verifying customer Order with enter valid and invalid Customer Details with Name, Mobile, Alt No, Email, Flat No and Locality")
	@Story("Story Name : To check and Verify customer Order with enter valid and invalid Customer Details with Name, Mobile, Alt No, Email, Flat No and Locality")
	@Link("Customer App 5.2.1.apk")
	public void customerProfileDetails(String name, String email, String alternateNo) throws Exception {

		// click on menu link
		profilePage.MenuLink();

		// click on my my profile link
		profilePage.profileLink();

		// click on edit icon
		profilePage.editlink();

		// Created list for Customer Details
		List<String> elements = new ArrayList<String>();

		// enter name
		elements.add(name);

		// enter email id
		elements.add(email);

		// enter alternate no
		elements.add(alternateNo);

		profilePage.customerProfileDetails(elements);

		// click on done button
		profilePage.doneButton();

		Thread.sleep(2000);

		String ActualResult = profilePage.validateValidationMessage();

		if (ActualResult == "Profile") {

			System.out.println(" Profile screen text");
			Assert.assertEquals(ActualResult, "Profile");

		}

		Assert.assertEquals(ActualResult, "Profile", "Profile text screen is not displaying");

	}

	/*
	 * // Created list for Customer Details List<String> elementss = new
	 * ArrayList<String>();
	 * 
	 * elementss.add(flatNo);
	 * 
	 * elementss.add(locality);
	 * 
	 * elementss.add(landmark);
	 * 
	 * profilePage.editAddresslink();
	 * 
	 * profilePage.customerAddressDetails(elementss);
	 * 
	 * profilePage.doneButton();
	 * 
	 * // profilePage.skipPopup();
	 * 
	 * assertFalse(true, "invalid message");
	 * 
	 * 
	 * }
	 * 
	 */

	@Test(enabled = false, description = "Verifying Existing Customer Login and also Verifying LogOut")
	@Severity(SeverityLevel.NORMAL)
	@Description("Test Case Description: Verifying Existing Customer Login and also Verifying LogOut")
	@Story("Story Name : To check and Verify Existing Customer Login and also Verifying LogOut")
	public void morelink() throws Exception {

		// Enter Valid Mobile Number and Otp
		// existingCustomerPage.existingCustomerDetails("8800735367", "12468");

		Thread.sleep(5000);

		// profilePage.moreLink();

		Thread.sleep(5000);

	}

	@AfterMethod(description = "Andriod Devices Tear Down")
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}

}
