package com.customer_app.qa.testcases;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.customer_app.qa.base.TestBase;
import com.customer_app.qa.listeners.CustomerAllureListeners;
import com.customer_app.qa.pages.CalenderPage;
import com.customer_app.qa.pages.MyPlanPage;
import com.customer_app.qa.pages.ProfilePage;
import com.customer_app.qa.utilities.CalenderUtility;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ CustomerAllureListeners.class })
@Feature("My Plan")
@Epic("Regression Tests")
public class MyPlanPageTest extends TestBase {

	MyPlanPage myPlanPage;
	ProfilePage profilePage;
	CalenderPage calenderPage;
	
	CalenderUtility calenderutility;

	public MyPlanPageTest() throws IOException {
		super();

	}

	
	@BeforeMethod
	public void setUp() throws Exception {

		initiallization();

		myPlanPage = new MyPlanPage();
		profilePage = new ProfilePage();
		calenderPage = new CalenderPage();
		
		calenderutility=new CalenderUtility();

	}
	
	
	@Test(enabled = false,priority = 0, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on my plan Banner icon on homepage ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to Banner section and click on my plan Banner icon on homepage")
	@Story("Story Name : click on my plan Banner icon on homepage")
	public void clickRandomlyHorizontalProductNameList() throws Exception {

		
		myPlanPage.randomSelectHorizontalProductList();
		Thread.sleep(3000);

	}


	@Test(enabled=false,priority = 0, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on my plan Banner icon on homepage ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to Banner section and click on my plan Banner icon on homepage")
	@Story("Story Name : click on my plan Banner icon on homepage")
	public void fetchingProductNamePrices() throws Exception {

		
		myPlanPage.fetchingProductListNameandPrices();
		Thread.sleep(3000);
		

	}

	
	@Test(priority = 0, description = "Sceanrio is for placeorder for anu milk product ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to Banner section and click on my plan Banner icon on homepage")
	@Story("Story Name : click on my plan Banner icon on homepage")
	public void orderPlaceMilkProduct() throws Exception {
		

		myPlanPage.randomSelectHorizontalProductList();
		Thread.sleep(3000);
		
		myPlanPage.orderPlaceMilkProducts();
		Thread.sleep(3000);
		
		
	
	}
	
	
	
	
	

	/**
	 * Scenario is to validate arrival of My Plan page functionality and click on my
	 * plan Banner icon on home page.
	 */

	@Test(enabled = false, priority = 0, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on my plan Banner icon on homepage ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Move to Banner section and click on my plan Banner icon on homepage")
	@Story("Story Name : click on my plan Banner icon on homepage")
	public void clickOnMyPlanBanner() throws Exception {

		// click on Banner My PLan
		myPlanPage.clickOnMyPlanBanner();
		Thread.sleep(3000);

	}

	/**
	 * Scenario is to validate arrival of My Plan page functionality and click on
	 * OrderNow Banner section on home page
	 */

	@Test(enabled = false, priority = 1, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on OrderNow Banner section on homepage ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Swipe banner left to right and move to OrderNow Banner and click on OrderNow section on homepage")
	@Story("Story Name : click on Order Now Banner section")
	public void clickOnOrderNowBanner() throws Exception {

		Thread.sleep(5000);
		// click on Banner Order Now
		myPlanPage.clickOnOrderNowBanner();
		Thread.sleep(3000);

	}

	/**
	 * Scenario is to validate arrival of My Plan page functionality and click on
	 * Order click on Add Subscription button
	 * 
	 */

	@Test(enabled = false, priority = 2, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on Order click on Add Subscription button ")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: click on Menu Iocn First and move to Support section & click it and click on getting started section and again click on PLacorder button and finally click on Add Subscription Button")
	@Story("Story Name : click on Order click on Add Subscription button")
	public void clickOnAddSubscriptionButton() throws Exception {

		Thread.sleep(4000);
		// click on Menu Iocn
		myPlanPage.clickOnMenuIcon();

		// click on support section
		myPlanPage.clickOnSupportButton();

		// click on getting started section
		myPlanPage.clickOnGettingStartedSection();

		// click on placorder button
		myPlanPage.clickOnPlaceAnOrder();

		// click on Add Subscription Button
		myPlanPage.clickOnAddSubscriptionButton();
		Thread.sleep(3000);

	}

	/**
	 * Scenario is to validate arrival of My Plan page functionality and click on My
	 * PLan Icon
	 */

	@Test(enabled = false, priority = 3, description = "Sceanrio is to validate arrvial of My Plan page funcationality and click on My PLan Icon")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: LOgin First and click on My PLan Icon")
	@Story("Story Name : click on My PLan Icon")
	public void clickOnMyPlanIcon() throws Exception {

		// click on MY PLan Iocn
		myPlanPage.myplanIcon();
		Thread.sleep(3000);

	}

	/**
	 * to validate clicking on one by one products like buffalo milk first thanafter
	 * Desi ghee, Brown bread and protein white egg in MyPlan Page.
	 */

	@Test(enabled = false, priority = 4, description = "to validate clicking on one by one products like buffalo milk, DEsi ghee, Brown bread and protein white egg in MyPlan Page")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: clicking on one by one products like buffalo milk, DEsi ghee, Brown bread and protein white egg in MyPlan Page")
	@Story("Story Name : clicking on one by one products like buffalo milk, DEsi ghee, Brown bread and protein white egg in MyPlan Page")
	public void clickOnBuffaloMilkSection() throws Exception {

		Thread.sleep(4000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

		// click on desi ghee section
		myPlanPage.desiDanedarGheeSection();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

		// click on protein white egg section
		myPlanPage.proteinWhitEggSection();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

		// click on brown bread section
		myPlanPage.brownBreadSection();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(4000);

		// Assertion Actual Result Expected Result
		assertEquals("Navigate to MyPlan page", "Navigate to MyPlan page");

	}

	/**
	 * Verify that without created profile and Select buffalo milk and click on Add
	 * Plus button with single time and select Daily Days and select start Date and
	 * End Date optional and finally click on Confirm text link Profile page should
	 * display
	 */

	@Test(enabled = false, priority = 5, description = "Verify that Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and finally click on Confirm text link")
	@Story("Story Name : To check and Verify that Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and finally click on Confirm text link")
	public void buffaloMilkPlaceorder() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.addButton();

		// click on Confirm link
		myPlanPage.confirmButton();

		// Assertion Actual Result Expected Result
		assertEquals("Profile Page is open", "Profile Page is open");

		Thread.sleep(4000);

	}

	/**
	 * Verify that without created profile and Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and 
	 * finally click on Confirm text link Profile page should display and enter all mandatory filed
	 */

	@Test(enabled = false, priority = 6, description = "Verify that without created profile and Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and finally click on Confirm text link")
	@Story("Story Name : To check and Select buffalo milk and click on Add Plus button with single time and select Daily Days and select start Date and End Date optional and finally click on Confirm text link")
	public void placeOrderWithoutCreatedProfile() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.addButton();

		// click on Confirm link
		myPlanPage.confirmButton();

		// Assertion Actual Result Expected Result
		assertEquals("Profile Page is open", "Profile Page is open");

		// Enter Profile Details
//		profilePage.profileDetail("suraj mahato", "suraj@gmail.com", "8800735367", "flatNo-200", "Spaze-It-park",
//				"near by station");

		// click on skip button
		profilePage.skipPopup();
		Thread.sleep(8000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(4000);

		String expected = myPlanPage.expectedQty();

		assertTrue(true, expected);

	}

	/**
	 * click on plus icon with one time and click on confirm text link
	 */

	@Test(enabled = false, priority = 7, description = "Verify that click on plus icon with one time and click on confirm text link for Buffalo milk")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that click on plus icon with one time and click on confirm text link for Buffalo milk")
	@Story("Story Name : To check and click on plus icon with one time and click on confirm text link for Buffalo milk")
	public void updateQtyOfBuffaloMilk() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.updateQty();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(4000);

	}

	/**
	 * click on plus icon with multiple times(i.e 5 times) and click on confirm text
	 * link
	 */

	@Test(enabled = false, priority = 8, description = "Verify that click on plus icon with multiple times and click on confirm text link for Buffalo milk")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that click on plus icon with multiple times and click on confirm text link for Buffalo milk")
	@Story("Story Name : To check and click on plus icon with multiple times and click on confirm text link for Buffalo milk")
	public void updateMultipleTimesQtyOfBuffaloMilk() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.updateMultipleTimes();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(4000);

	}

	/**
	 * click on minus icon with multiple times(i.e 5 times) and click on confirm
	 * text link
	 */

	@Test(enabled = false, priority = 9, description = "Verify that click on minus icon with multiple times and click on confirm text link for Buffalo milk")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that click on minus icon with multiple times and click on confirm text link for Buffalo milk")
	@Story("Story Name : To check and click on minus icon with multiple times and click on confirm text link for Buffalo milk")
	public void deleteMultipleTimesQtyOfBuffaloMilk() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.deleteMultipleTimes();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(4000);

	}

	/**
	 * select end Date and click on confirm text link
	 */

	@Test(enabled = false, priority = 10, description = "Verify that select end dates and click on confirm text link for Buffalo milk")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that select end dates and click on confirm text link for Buffalo milk")
	@Story("Story Name : To check and select end dates and click on confirm text link for Buffalo milk")
	public void selectEndDateForBuffaloMilk() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.updateQty();

		// scroll down
		myPlanPage.swipScreen();

		// click on endat section
		myPlanPage.endAtSection();

		// click on ok button
		myPlanPage.clickOnOkButton();

		// select a particular date
		myPlanPage.selectParticularDate();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();

		// click on Home icon
		calenderPage.clickOnHomeIcon();

		// move to home page and click on right arrow icon and navigate to 2 month
		calenderPage.maincalenderEndMonthSwipe("July 2020");

		Thread.sleep(2000);

		// move to home page and click on left arrow icon and navigate back to current
		// month
		calenderPage.maincalenderStartMonthSwipe("May 2020");

		Thread.sleep(2000);

	}

	/**
	 * click on minus icon with one time and click on confirm text link
	 */

	@Test(enabled = false, priority = 11, description = "Verify that click on minus icon with one time and click on confirm text link for Buffalo milk")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that click on minus icon with one time and click on confirm text link for Buffalo milk")
	@Story("Story Name : To check and click on minus icon with one time and click on confirm text link for Buffalo milk")
	public void deleteMultipleTimesQtyOfBuffaloMilk1() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.buffaloMilkSections();

		// click on Add Button
		myPlanPage.deleteQty();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(4000);

	}

	/**
	 * whenever wallet balance is : 1500 Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and
	 * finally click on Confirm text link. 
	 * Expected Result: Your order placed sucessfully, In My Plan Desi Danedar ghee
	 * update with qty=1
	 */

	@Test(enabled = false, priority = 12, description = "Verify that Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and finally click on Confirm text link")
	@Story("Story Name : To check and Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and finally click on Confirm text link")
	public void placeOrderForDesiDanedarGhee() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.desiDanedarGheeSection();

		// click on Add Button
		myPlanPage.addButton();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(2000);

		assertTrue(true, "Your order is successfully placed");

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

		// click on Home icon
		calenderPage.clickOnHomeIcon();
		Thread.sleep(3000);

	}

	/**
	 * whenever wallet balance is : 1500 whenever subscription is already running for desi ghee with qty=1 and again adding qty=1 thanafter select Daily Days
	 * and select start Date and finally click on Confirm text link. 
	 * Expected Result: Your order placed sucessfully, In My Plan Desi Danedar ghee update with qty=2
	 */

	@Test(enabled = false, priority = 13, description = "Verify that Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and finally click on Confirm text link")
	@Story("Story Name : To check and Select DesiDanedar ghee and click on Add Plus button with single time and select Daily Days and select start Date and finally click on Confirm text link")
	public void placeOrderForDesiDanedarGhee1() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.desiDanedarGheeSection();

		// click on Add Button
		myPlanPage.updateQty();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(2000);

		assertTrue(true, "Your order is successfully placed");

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

		// click on Home icon
		calenderPage.clickOnHomeIcon();
		Thread.sleep(3000);

	}

	/**
	 * whenever wallet balance is : 1500 whenever subscription is already running for desi ghee with qty=2 and 
	 * again click on minus button with one time i.e qty=1 thanafter select one time Days and select delivery Date and finally click on Confirm text link. 
	 * Expected Result: Your order placed sucessfully, In My Plan Desi Danedar ghee update with qty=1
	 */

	@Test(enabled = false, priority = 14, description = "Verify that Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	@Story("Story Name : To check and Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	public void placeOrderForDesiDanedarGhee2() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.desiDanedarGheeSection();

		// click on Add Button
		myPlanPage.deleteQty();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(2000);

		assertTrue(true, "Your order has been updated");

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

	}

	/**
	 * whenever wallet balance is : 1500 whenever subscription is already running for desi ghee with qty=1 and 
	 * again click on plus button with 2 times i.e qty=3 thanafter select Daily Days and select start Date and finally click on Confirm text link. 
	 * Expected Result: Payment page should display, with amount=1500
	 */

	@Test(enabled = false, priority = 15, description = "Verify that Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	@Story("Story Name : To check and Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	public void placeOrderForDesiDanedarGhee3() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.desiDanedarGheeSection();

		// click on Add Button
		myPlanPage.updateQty();
		Thread.sleep(1000);

		// click on Add Button
		myPlanPage.updateQty();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(2000);

		// click on ok button
		myPlanPage.clickOnOkButton();
		Thread.sleep(6000);

		String expected = myPlanPage.expectedPayAmount();

		assertTrue(true, expected);

	}

	/**
	 * whenever wallet balance is : 1500 whenever subscription is already running for desi ghee with qty=1 and 
	 * again click on plus button with one times i.e qty=2 thanafter changing delivery dates with new. and
	 * and finally click on Confirm text link. 
	 * Expected Result: Your order placed sucessfully, In My Plan Desi Danedar ghee update with qty=2
	 */

	@Test(enabled=false,priority = 16, description = "Verify that Select DesiDanedar ghee and click on plus button with one time and select One Time Days and changing the delivery Date and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select DesiDanedar ghee and click on plus button with one time and  select One Time Days and changing the delivery Date and finally click on Confirm text link")
	@Story("Story Name : To check and Select DesiDanedar ghee and click on plus button with one time and  select One Time Days and changing the delivery Date and finally click on Confirm text link")
	public void placeOrderForDesiDanedarGhee4() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.desiDanedarGheeSection();

		// click on Add Button
		myPlanPage.updateQty();
		Thread.sleep(1000);

		// scroll down
		myPlanPage.swipScreen();

		// click on Delivery Date section
		myPlanPage.deliverDateSection();

		// select a particular date
		myPlanPage.selectParticularDateForDesiGhee();

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(3000);

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();

		// click on Home icon
		calenderPage.clickOnHomeIcon();

		// move to home page and click on right arrow icon and navigate to 2 month
		calenderPage.maincalenderEndMonthSwipe("July 2020");

		Thread.sleep(2000);

		// move to home page and click on left arrow icon and navigate back to current month
		calenderPage.maincalenderStartMonthSwipe("May 2020");

		Thread.sleep(2000);

	}
	

	/**
	 * whenever wallet balance is : 1500 whenever subscription is already running for desi ghee with qty=2 and 
	 * again click on minus button with 2 times i.e qty=0 thanafter select one time Days and select delivery Date and finally click on Confirm text link. 
	 * Expected Result: Your Order Updated sucessfully. In My Plan Page desi ghee update with qty=0 and In calender page it will not display upcoming icon.
	 */

	@Test(enabled=false, priority = 17, description = "Verify that Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	@Story("Story Name : To check and Select DesiDanedar ghee and click on minus button with one time and select Daily Days and changing the start Date and finally click on Confirm text link")
	public void placeOrderForDesiDanedarGhee5() throws Exception {

		Thread.sleep(5000);

		// click on myplan icon
		myPlanPage.myplanIcon();

		// click on buffalo milk section
		myPlanPage.desiDanedarGheeSection();

		// click on Add Button
		myPlanPage.deleteQty();
		Thread.sleep(2000);
		
		// click on Add Button
		myPlanPage.deleteQty();
		

		// click on Confirm link
		myPlanPage.confirmButton();
		Thread.sleep(2000);

		assertTrue(true, "Your order has been updated");

		// Click on left arrow icon in edit plan page
		myPlanPage.leftArrowIcon();
		Thread.sleep(2000);

	}

	@Test(enabled=false,priority = 10, description = "Verify that click on minus icon with multiple times and click on confirm text link for Buffalo milk")
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Case Description: Verify that click on minus icon with multiple times and click on confirm text link for Buffalo milk")
	@Story("Story Name : To check and click on minus icon with multiple times and click on confirm text link for Buffalo milk")
	public void randomproductSelection() throws Exception {

		Thread.sleep(5000);

//		calenderutility.datePickerTest();

		Thread.sleep(6000);

	}

	@AfterMethod
	public void End() {
		if (driver != null) {
			driver.quit();

		}

	}

}
