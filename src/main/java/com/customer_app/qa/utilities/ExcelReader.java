package com.customer_app.qa.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.customer_app.qa.base.TestBase;

public class ExcelReader extends TestBase {

	
	
    public static String TESTDATA_SHEET_PATH="/home/dell/eclipse-workspace/customer-app-automation"
    		+ "/src/main/resources/excel/profileDetails.xlsx";
	
	
	static Workbook book;
	static Sheet sheet;
	
	public static Object[][] getTestData(String sheetName){
		
		FileInputStream file=null;
		
		try {
			file=new FileInputStream(TESTDATA_SHEET_PATH);
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
			
		}
		try {
			book=WorkbookFactory.create(file);
//		}catch(InvalidFormatException e) {
//			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
			
		}
		sheet=book.getSheet(sheetName);
		
		Object[][] data= new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		

		for(int i=0;i<sheet.getLastRowNum();i++) {
			for(int k=0;k<sheet.getRow(0).getLastCellNum();k++) {
				//System.out.println(i);
				//System.out.println(sheet.getFirstRowNum()+);
				if(sheet.getRow(i +1).getCell(k)!=null) {
				data[i][k]=sheet.getRow(i +1).getCell(k).toString();
				}else {
					data[i][k]="";
				}
				
			}
		}
		return data;

	}
}



