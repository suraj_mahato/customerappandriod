package com.customer_app.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.customer_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static java.time.Duration.ofSeconds;

import io.qameta.allure.Step;

public class InAppSupportPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@text='Menu']")
	WebElement menuIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Support']")
	WebElement SupportFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Delivery related issue']")
	WebElement DeliveryIssueFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Billing/Payment/Refund related issue']")
	WebElement BillingPaymentRefundIssueFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Quality Related Issue']")
	WebElement QualityIssueFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Other Issue']")
	WebElement OtherIssueFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Profile related requests']")
	WebElement ProfileRelatedRequestsFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Call us (6:00AM-9:00PM)']")
	WebElement CallUsFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Email Us']")
	WebElement EmailUsFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Order is not delivered']")
	WebElement OrderIsNotDeliveredFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Product(s) Missing']")
	WebElement productMissingFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Wrong product/quantity delivered']")
	WebElement WrongproductFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Other Delivery Requests']")
	WebElement otherDeliveryRequestFiledXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Previous Menu']")
	WebElement PreviousMenuFiledXpath;

	@FindBy(xpath = "//android.widget.Button[@text='YES']")
	WebElement YesButtonXpath;

	@FindBy(xpath = "//android.widget.Button[@text='SUBMIT']")
	WebElement submitButtonXpath;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement backIconXpath;
	
	@FindBy(xpath= "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/editTextPhoneNumber']")
	WebElement enterNewNoFiledXpath;
	
	@FindBy(xpath = "//android.widget.Button[@text='ADD FLAT NO / HOUSE NO / BUILDING']")
	WebElement AddFlatNoButtonXpath;
	
	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_flat']")
	WebElement flatNo_Xpath;
	
	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_locality']")
	WebElement DeliveryLocation_Xpath;	
	
	@FindBy(xpath = "//android.widget.Button[@resource-id='app.mycountrydelight.in.countrydelight:id/btn_save']")
	WebElement SaveAddressButtonXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='My Profile']")
	WebElement MyProfileLinkXpath;
	
	@FindBy(xpath= "//android.widget.Button[@text='CALL US']")
	WebElement callusButtonXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@resource-id='app.mycountrydelight.in.countrydelight:id/textViewOpenConversations']")
	WebElement OpenChatFiledXpath;
	
	
	WebElement topicFiled;
	
	
	

	// Initializing the page objects
	public InAppSupportPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("clicking on menu icon text ")
	public void clickOnMenuIcon() throws Exception {

		// explicit wait - to wait for the menu button to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Menu']")));

		// click on the menu button as soon as the "Menu" button is visible
		menuIconXpath.click();

	}

	@Step("clicking on support filed ")
	public void clickOnSupportFiled() throws Exception {
		
		
		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector()" 
				+ ".resourceId(\"app.mycountrydelight.in.countrydelight:id/rv_options\")).scrollIntoView("
				+ " new UiSelector().textMatches(\"Support\").instance(0))");
		
		
		// explicit wait - to wait for the support filed button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Support']")));

		// click on the support filed button as soon as the "Support" filed button is
		// visible
		SupportFiledXpath.click();

	}

	@Step("click on delivery issue")
	public void clickonDeliveryIssue() throws Exception {

		// explicit wait - to wait for the support filed button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Delivery related issue']")));

		// click on the delivery issue filed button as soon as the "delivery issue" filed button is visible
		DeliveryIssueFiledXpath.click();

	}
	

	@Step("click on Billing issue filed")
	public void clickonBillingIssue() throws Exception {

		// explicit wait - to wait for the billing filed button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Billing/Payment/Refund related issue']")));

		// click on the billing issue filed button as soon as the "billing issue" filed button is visible
		BillingPaymentRefundIssueFiledXpath.click();

	}
	

	@Step("click on Profile related requests filed")
	public void clickonProfileRelatedRequest() throws Exception {

		// explicit wait - to wait for the Profile related requests filed button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Profile related requests']")));

		// click on the Profile related requests filed button as soon as the "Profile related requests" filed button is visible
		ProfileRelatedRequestsFiledXpath.click();
		

	}
	
	

	@Step("clicking on call us(6: 00 AM-9:00 PM) filed ")
	public void clickOnCallUsFiled() throws Exception {
		
		
		MobileElement els = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"Call us (6:00AM-9:00PM)\"));");
		
		
		// explicit wait - to wait for the call us filed to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Call us (6:00AM-9:00PM)']")));

		// click on the call us filed as soon as the "Call Us" filed is visible
		
		CallUsFiledXpath.click();
		

	}
	
	@Step("clicking on call us button ")
	public void clickOnCallUsButton() throws Exception {	
		
		// explicit wait - to wait for the call us button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@text='CALL US']")));

		// click on the call us button as soon as the "Call Us" button is visible
		
		callusButtonXpath.click();
		

	}
	
	
	@Step("clicking on email us filed ")
	public void clickOnEmailUsFiled() throws Exception {
		
		
		MobileElement els = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"Email Us\"));");
		
		
		// explicit wait - to wait for the email us filed to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Email Us']")));

		// click on the call us filed as soon as the "Email Us" filed is visible
		
		EmailUsFiledXpath.click();
		

	}
	
	
	
	

	@Step("click on submit button")
	public void clickonSUbmitButton() throws Exception {

		// explicit wait - to wait for the submit button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@text='SUBMIT']")));

		// click on the delivery issue filed button as soon as the "Submit" button is
		// visible
		submitButtonXpath.click();

	}
	
	@Step("click on Add flat no/ house No/Building button")
	public void clickonAddFlatnOButton() throws Exception {

		// explicit wait - to wait for Add flat no/ house No/Building button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@text='ADD FLAT NO / HOUSE NO / BUILDING']")));

		// click on the Add flat no/ house No/Building button as soon as the " Add flat no/ house No/Building " button is visible
		AddFlatNoButtonXpath.click();
       
		
	}
	
	@Step("click on save address button")
	public void clickonSaveAddressButton() throws Exception {

		// explicit wait - to wait for save address button to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@resource-id='app.mycountrydelight.in.countrydelight:id/btn_save']")));

		// click on the save address button as soon as the " save address" button is visible
		SaveAddressButtonXpath.click();
       
		
	}
	
	@Step("click on my profile filed")
	public void clickonMyProfileFiled() throws Exception {

		// explicit wait - to wait for my profile filed to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='My Profile']")));

		// click on the my profile filed as soon as the " my profile " filed is visible
		MyProfileLinkXpath.click();
       
		
	}

	@Step("click on Open Chat filed")
	public void clickonOPenChatFiled() throws Exception {

		// explicit wait - to wait for my open chat filed to be click-able

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@resource-id='app.mycountrydelight.in.countrydelight:id/textViewOpenConversations']")));

		// click on the open chat filed as soon as the " OPen Chat " filed is visible
		OpenChatFiledXpath.click();
		
		
		
	}
	
		

	@Step("click one by one all topics")
	public void clickOneByOneAllTopics() throws Exception {

		// finding list of FAQ filed
		List<WebElement> elements = driver.findElements(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup"));

		System.out.println("list count:" + elements.size());
		
		for (int i = 1; i < elements.size(); i++) {

			System.out.println("Print of i: " + i);

			// click on text by one by one
			
			String index = Integer.toString(i);

            switch (index) {
            
        	case "1":

    			WebElement AllTopicFiledClick = driver.findElement(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

    			// explicit wait - to wait for the First filed button to be click-able
    			WebDriverWait wait = new WebDriverWait(driver, 30);
    			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));
 			  			
    			//tap action
    			TouchAction t = new TouchAction(driver);
    			t.tap(tapOptions().withElement(element(AllTopicFiledClick))).perform();
    			
       			
    			// explicit wait - to wait for the First filed button to be click-able
    			WebDriverWait waits = new WebDriverWait(driver, 30);
    			waits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));
    
    			
    			//click on back icon
    			backIconXpath.click();
    			
    			//click on popup yes button
    			YesButtonXpath.click();

    			break;
    			
        	case "2":

    			WebElement AllTopicFiledClick2 = driver.findElement(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

    			// explicit wait - to wait for the second filed button to be click-able
    			WebDriverWait wait2 = new WebDriverWait(driver, 30);
    			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));
 			
    			//tap action click on the second filed button as soon as the second filed button is visible
    			TouchAction t2 = new TouchAction(driver);
    			t2.tap(tapOptions().withElement(element(AllTopicFiledClick2))).perform();
    			
    			
    			// explicit wait - to wait for the back icon filed button to be click-able
    			WebDriverWait waitss = new WebDriverWait(driver, 30);
    			waitss.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));
   		
    			//click on back icon
    			backIconXpath.click();
    			
    			//click on popup yes button
    			YesButtonXpath.click();

    			break;
    			
        	case "3":

    			WebElement AllTopicFiledClick3 = driver.findElement(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

    			// explicit wait - to wait for the 3rd filed button to be click-able
    			WebDriverWait wait3 = new WebDriverWait(driver, 30);
    			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));
			
    			// Tap action by click on the 3rd filed button as soon as the date filed button is visible
    			TouchAction t3 = new TouchAction(driver);
    			t3.tap(tapOptions().withElement(element(AllTopicFiledClick3))).perform();
    			
    			
    			// explicit wait - to wait for the back icon filed button to be click-able
    			WebDriverWait waitsss = new WebDriverWait(driver, 30);
    			waitsss.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));
    			
    			//click on back icon
    			backIconXpath.click();
    			
    			//click on popup yes button
    			YesButtonXpath.click();

    			break;
    			
        	case "4":

    			WebElement AllTopicFiledClick4 = driver.findElement(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

    			// explicit wait - to wait for the Fourth filed button to be click-able
    			WebDriverWait wait4 = new WebDriverWait(driver, 30);
    			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
    					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));
 
    			// Tap action by click on the date filed button as soon as the date filed button is visible
    			TouchAction t4 = new TouchAction(driver);
    			t4.tap(tapOptions().withElement(element(AllTopicFiledClick4))).perform();
    			
    			// explicit wait - to wait for the back icon filed button to be click-able
    			WebDriverWait waitssss = new WebDriverWait(driver, 30);
    			waitssss.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));
    			
    			//click on back icon
    			backIconXpath.click();
    			
    			//click on popup yes button
    			YesButtonXpath.click();
    			

    			break;

           	case "5":

        			WebElement AllTopicFiledClick5 = driver.findElement(By.xpath(
        					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

        			// explicit wait - to wait for the 5th filed button to be click-able
        			WebDriverWait wait5 = new WebDriverWait(driver, 30);
        			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
        					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));
     			
        			// Tap Action by click on the 5th filed button as soon as the 5th filed button is visible
        			TouchAction t5 = new TouchAction(driver);
        			t5.tap(tapOptions().withElement(element(AllTopicFiledClick5))).perform();
        			     			
        			// explicit wait - to wait for the back icon filed button to be click-able
        			WebDriverWait wait5s = new WebDriverWait(driver, 30);
        			wait5s.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));
   			
        			backIconXpath.click();

        			break;
        			
        			
           	case "6":

        			WebElement AllTopicFiledClick6 = driver.findElement(By.xpath(
        					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

        			// explicit wait - to wait for the 6th filed button to be click-able
        			WebDriverWait wait6 = new WebDriverWait(driver, 30);
        			wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
        					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));
        			      			
        			// click on the 6th filed button as soon as the date filed button is visible
        			TouchAction t6 = new TouchAction(driver);
        			t6.tap(tapOptions().withElement(element(AllTopicFiledClick6))).perform();
        			   			
           			// explicit wait - to wait for the back icon filed button to be click-able
        			WebDriverWait wait6s = new WebDriverWait(driver, 30);
        			wait6s.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));
        			
        			backIconXpath.click();

        			break;

    			
        	default:

    			System.out.println("There is no Date is displaying");
    
            
            
            }
            

            
		}
	}
	

	@Step("click one by one option other delivery requests")
	public void clickOneByOneOptionOfOtherDeliveryRequest() throws Exception {

		// finding list of FAQ filed
		List<WebElement> elements = driver.findElements(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView"));

		System.out.println("list count:" + elements.size());

		for (int i = 1; i <= elements.size(); i++) {

			System.out.println("Print of i: " + i);

			// click on text by one by one

			String index = Integer.toString(i);

			switch (index) {

			case "1":

				WebElement FirstOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the First filed button to be click-able
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[\" + index + \"]")));

				// tap action click on the first filed button as soon as the first is visible	
               FirstOPtion.click();
               
               //Are you satisfied with our support
               ValidateAreYouSatisfiledWithOurSupport("1");
               
   			
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "2":

				WebElement secondOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the second filed button to be click-able
				WebDriverWait wait2 = new WebDriverWait(driver, 30);
				wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[\" + index + \"]")));

				// tap action click on the second filed button as soon as the first is visible	
				secondOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "3":

				WebElement thirdOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the third filed button to be click-able
				WebDriverWait wait3 = new WebDriverWait(driver, 30);
				wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[\" + index + \"]")));

				// tap action click on the third filed button as soon as the third is visible	
				thirdOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();
						

				break;

			case "4":

				WebElement fourthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the second filed button to be click-able
				WebDriverWait wait4 = new WebDriverWait(driver, 30);
				wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[\" + index + \"]")));

				// tap action click on the second filed button as soon as the first is visible	
				fourthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");	
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "5":

				WebElement fifthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the second filed button to be click-able
				WebDriverWait wait5 = new WebDriverWait(driver, 30);
				wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[\" + index + \"]")));

				// tap action click on the second filed button as soon as the first is visible	
				fifthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "6":

				WebElement sixthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the second filed button to be click-able
				WebDriverWait wait6 = new WebDriverWait(driver, 30);
				wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[\" + index + \"]")));

				// tap action click on the second filed button as soon as the first is visible	
				sixthOPtion.click();
				
    			//click on back icon
    			backIconXpath.click();
    			
    			//click on popup yes button
    			YesButtonXpath.click();

				break;

			default:

				System.out.println("There is no Date is displaying");

			}

			//click on delivery issue and order is is not delivered filed
            ValidateBySelectanyOneOptionFromDeliveryIssue("4");

			
		}
		

	}

	@Step("Select the Date")
	public void selectTheDate(String i) {

		switch (i) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			date.click();

			break;

		case "2":

			WebElement dates = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waits = new WebDriverWait(driver, 30);
			waits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			dates.click();

			break;

		case "3":

			WebElement datess = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waitss = new WebDriverWait(driver, 30);
			waitss.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			datess.click();

			break;

		case "4":

			WebElement dates4 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waits4 = new WebDriverWait(driver, 30);
			waits4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			dates4.click();

			break;

		case "5":

			WebElement dates5 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waits5 = new WebDriverWait(driver, 30);
			waits5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			dates5.click();

			break;

		case "6":

			WebElement dates6 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waits6 = new WebDriverWait(driver, 30);
			waits6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			dates6.click();

			break;

		case "7":

			WebElement dates7 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waits7 = new WebDriverWait(driver, 30);
			waits7.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			dates7.click();

			break;

		case "8":

			WebElement dates8 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]"));

			// explicit wait - to wait for the date filed button to be click-able
			WebDriverWait waits8 = new WebDriverWait(driver, 30);
			waits8.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ i + "]")));

			// click on the date filed button as soon as the date filed button is visible
			dates8.click();

			break;

		default:

			System.out.println("There is no Date is displaying");

		}

	}

	@Step("Select any one option of delivery issue option ")
	public void ValidateBySelectanyOneOptionFromDeliveryIssue(String index) throws Exception {

		// click on delivery issue
		clickonDeliveryIssue();

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date2.click();

			break;

		case "3":

			WebElement date3 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait3 = new WebDriverWait(driver, 30);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date3.click();

			break;

		case "4":

			WebElement date4 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait4 = new WebDriverWait(driver, 30);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date4.click();

			break;

		case "5":

			WebElement date5 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait5 = new WebDriverWait(driver, 30);
			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date5.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}
	
	@Step("Select any one option of Billing/payment/Refund realted issue option ")
	public void ValidateBySelectanyOneOptionFromBillingPaymentIssue(String index) throws Exception {

		// click on billing issue filed
		clickonBillingIssue();

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the first filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the first filed button as soon as the first filed button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for second filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the second filed button as soon as the any second option filed
			// button is visible
			date2.click();

			break;

		case "3":

			WebElement date3 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the third filed button to be click-able
			WebDriverWait wait3 = new WebDriverWait(driver, 30);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the third filed button as soon as the third button is visible
			date3.click();

			break;

		case "4":

			WebElement date4 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for fourth filed button to be click-able
			WebDriverWait wait4 = new WebDriverWait(driver, 30);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the fourth filed button as soon as the fourth button is visible
			date4.click();

			break;

		case "5":

			WebElement date5 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for fifth filed button to be click-able
			WebDriverWait wait5 = new WebDriverWait(driver, 30);
			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the fifth filed button as soon as the fifth filed button is visible
			date5.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}
	
	@Step("Select any one option of profile related requests ")
	public void ValidateBySelectanyOneOptionFromProfileRelatedRequests(String index) throws Exception {

		// profile related issue filed
    	clickonProfileRelatedRequest();

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the first filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the first filed button as soon as the first filed button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for second filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the second filed button as soon as the any second option filed
			// button is visible
			date2.click();

			break;

		case "3":

			WebElement date3 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the third filed button to be click-able
			WebDriverWait wait3 = new WebDriverWait(driver, 30);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the third filed button as soon as the third button is visible
			date3.click();

			break;

		case "4":

			WebElement date4 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for fourth filed button to be click-able
			WebDriverWait wait4 = new WebDriverWait(driver, 30);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the fourth filed button as soon as the fourth button is visible
			date4.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Select any one option of other delivery Request from below  ")
	public void ValidateBySelectFromOtherDeliveryRequestsOption(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date2.click();

			break;

		case "3":

			WebElement date3 = driver.findElement(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait3 = new WebDriverWait(driver, 30);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date3.click();

			break;

		case "4":

			WebElement date4 = driver.findElement(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait4 = new WebDriverWait(driver, 30);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date4.click();

			break;

		case "5":

			WebElement date5 = driver.findElement(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait5 = new WebDriverWait(driver, 30);
			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date5.click();

			break;

		case "6":

			WebElement date6 = driver.findElement(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait6 = new WebDriverWait(driver, 30);
			wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date6.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Are you Satisfied with our support Yes Or No ? ")
	public void ValidateAreYouSatisfiledWithOurSupport(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Do you want to register a compliants Yes Or No ? ")
	public void ValidateDoYouWantToRegisterAComplaints(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Please select the product divison")
	public void ValidatePleaseSelectProductDivison(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView["
							+ index + "]")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}
	
	@Step("Can you please confirm the delivery ? Yes, Not Received")
	public void ValidatePleaseConfirmtheDelivery(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the Yes button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the yes filed button as soon as the yes button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the Not Received button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the Not Received button as soon as the Not Received button is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Please select the product for which you didn't receive any discount")
	public void ValidatePleaseSelectTheproductForwhichyoudidnotreceivediscount(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the select any one product to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the any one product filed button as soon as the any one product is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the select any one product to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the any one product as soon as the any one product is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}
	
	@Step("Please select the category of Cashback related issues")
	public void ValidatePleaseSelectCategoryOfCashbackRelatedIssues(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the first recharge cashback button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the first recharge cashback button as soon as the first recharge cashback button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the Referral cashback button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the Referral cashback button as soon as the Referral cashback button is visible
			date2.click();

			break;
			
		case "3":

			WebElement date3 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

			// explicit wait - to wait for the previous menu button to be click-able
			WebDriverWait wait3 = new WebDriverWait(driver, 30);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

			// click on the previous menu button as soon as the previous menu button is visible
			date3.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}


	
	
	

	@Step("Select the Recent Orders(Select date you have issue with) ")
	public void ValidateRecentOrders(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));

			// click on the any one option filed button as soon as the any one option file
			// button is visible
			date2.click();

			break;

		case "3":

			WebElement date3 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView"));

			// explicit wait - to wait for the option filed button to be click-able
			WebDriverWait wait3 = new WebDriverWait(driver, 30);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]/androidx.appcompat.widget.LinearLayoutCompat/android.widget.TextView")));

			WebElement selectFirst = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/androidx.appcompat.widget.LinearLayoutCompat[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/androidx.appcompat.widget.LinearLayoutCompat"));

			TouchAction t = new TouchAction(driver);
			t.longPress(longPressOptions().withElement(element(selectFirst)).withDuration(ofSeconds(2)))
					.moveTo(element(date3)).release().perform();

			// click on the any one option filed button as soon as the any one option filed
			// button is visible
			date3.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Are you want to exist? Yes Or No ? ")
	public void AreYouSureWantToExist(String index) throws Exception {

		// explicit wait - to wait for the back button to be click-able
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		wait1.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")));

		// click on back button as soon as the any one option file button is visible

		backIconXpath.click();

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]"));

			// explicit wait - to wait for the Yes filed button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]")));

			// click on the any one Yes filed button as soon as the any one option file
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]"));

			// explicit wait - to wait for the No filed button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]")));

			// click on the any one No button as soon as the any one option filed button is
			// visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Please choose the products from below ")
	public void PleaseChoosetheProductsFromBelow(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]"));

			// explicit wait - to wait for the First filed radio button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.CheckBox["
							+ index + "]")));

			// click on the any First filed radio button as soon as the any one option file
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.CheckBox["
							+ index + "]"));

			// explicit wait - to wait for the First second radio button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.CheckBox["
							+ index + "]")));

			// click on the second filed radio button as soon as the second filed radio
			// button is visible
			date2.click();

			break;

		case "3":

			WebElement first = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.CheckBox[1]"));

			WebElement second = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.CheckBox[2]"));

			first.click();
			Thread.sleep(2000);

			second.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}

	@Step("Please enter quantity you received")
	public void PleaseUpdatetheQty(String index) throws Exception {

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]"));

			// explicit wait - to wait for the First filed radio button to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]")));

			// click on the any First filed radio button as soon as the any one option file
			// button is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]"));

			// explicit wait - to wait for the First second radio button to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.Button["
							+ index + "]")));

			// click on the second filed radio button as soon as the second filed radio
			// button is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}
	

	@Step("Please enter New number")
	public void enterNewNumber(String text) throws Exception {
		
		// explicit wait - to wait for the First second radio button to be click-able
		WebDriverWait wait2 = new WebDriverWait(driver, 30);
		wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/editTextPhoneNumber']")));
		
		//click on Please enter New number filed
		enterNewNoFiledXpath.click();
	
		// enter name
		Actions a = new Actions(driver);
		a.sendKeys(text);
		a.perform();
		
		
	}
	
	@Step("Enter flat No and Delivery location")
	public void UpdateFlatNoandDeliveryLocation(String ft, String dl) throws Exception {
		
		// explicit wait - to wait for flatNO filed to be click-able
		WebDriverWait wait2 = new WebDriverWait(driver, 30);
		wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_flat']")));
		
		//click on Flat No filed
		flatNo_Xpath.click();
	
		// enter flat no
		Actions a = new Actions(driver);
		a.sendKeys(ft);
		a.perform();
		
		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		// explicit wait - to wait for delivery location to be click-able
		WebDriverWait wait3 = new WebDriverWait(driver, 30);
		wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_locality']")));
		
		//click on delivery location filed
		DeliveryLocation_Xpath.click();
	
		// enter name
		Actions b = new Actions(driver);
		b.sendKeys(dl);
		b.perform();
		
		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		
		
	}
	

	@Step("click one by one option of stops all the services")
	public void clickOneByOneOptionOfStopsAlltheServices() throws Exception {

		// finding list of FAQ filed
		List<WebElement> elements = driver.findElements(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView"));

		System.out.println("list count:" + elements.size());

		for (int i = 1; i <= elements.size(); i++) {

			System.out.println("Print of i: " + i);

			// click on text by one by one

			String index = Integer.toString(i);

			switch (index) {

			case "1":

				WebElement FirstOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the quality button to be click-able
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the quality button as soon as the quality is visible	
               FirstOPtion.click();
               
               //Are you satisfied with our support
               ValidateAreYouSatisfiledWithOurSupport("1");
               
   			
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "2":

				WebElement secondOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the services button to be click-able
				WebDriverWait wait2 = new WebDriverWait(driver, 30);
				wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the services button as soon as the services is visible	
				secondOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "3":

				WebElement thirdOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the going out of station button to be click-able
				WebDriverWait wait3 = new WebDriverWait(driver, 30);
				wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the going out of station button as soon as the going out of station button is visible	
				thirdOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();
						

				break;

			case "4":

				WebElement fourthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the billing button to be click-able
				WebDriverWait wait4 = new WebDriverWait(driver, 30);
				wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the billing button as soon as the billing is visible	
				fourthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");	
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "5":

				WebElement fifthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the timing button to be click-able
				WebDriverWait wait5 = new WebDriverWait(driver, 30);
				wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the timing button as soon as the timing button is visible	
				fifthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;

			case "6":

				WebElement sixthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the others button to be click-able
				WebDriverWait wait6 = new WebDriverWait(driver, 30);
				wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the others button as soon as the others button is visible	
				sixthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;
				
			case "7":

				WebElement seventhOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the only for trail button to be click-able
				WebDriverWait wait7 = new WebDriverWait(driver, 30);
				wait7.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the only for trail button as soon as the only for trail button is visible	
				seventhOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;
				
			case "8":

				WebElement eighthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the not required button to be click-able
				WebDriverWait wait8 = new WebDriverWait(driver, 30);
				wait8.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the not required button as soon as the not required button is visible	
				eighthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;
				
			case "9":

				WebElement ninthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the out of delivery area button to be click-able
				WebDriverWait wait9 = new WebDriverWait(driver, 30);
				wait9.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the out of delivery area button as soon as the out of delivery area button is visible	
				ninthOPtion.click();
				
	            //Are you satisfied with our support
	            ValidateAreYouSatisfiledWithOurSupport("1");
	            
   		    	//click on back icon
   			    backIconXpath.click();

				break;
				
				
			case "10":

				WebElement tenthOPtion = driver.findElement(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]"));

				// explicit wait - to wait for the out of delivery area button to be click-able
				WebDriverWait wait10 = new WebDriverWait(driver, 30);
				wait10.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[" + index + "]")));

				// tap action click on the out of delivery area button as soon as the out of delivery area button is visible	
				tenthOPtion.click();
	            
   		    	//click on back icon
   			    backIconXpath.click();
   			    
    			//click on popup yes button
    			YesButtonXpath.click();
   			    

				break;

			default:

				System.out.println("There is no Date is displaying");

			}

			//click on delivery issue and order is is not delivered filed
			ValidateBySelectanyOneOptionFromProfileRelatedRequests("3");

			
		}
		
	}
	
	
		

		@Step("click one by one option of write to us screen")
		public void clickOneByOneOptionOfWriteToUsFiled() throws Exception {

			// finding list of FAQ filed
			List<WebElement> elements = driver.findElements(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup"));

			System.out.println("list count:" + elements.size());

			for (int i = 1; i <= elements.size(); i++) {

				System.out.println("Print of i: " + i);

				// click on text by one by one

				String index = Integer.toString(i);
				

				WebElement tenthOPtion = driver.findElement(By.xpath(
						"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]"));

				// explicit wait - to wait for the out of delivery area button to be click-able
				WebDriverWait wait10 = new WebDriverWait(driver, 30);
				wait10.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"	/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[" + index + "]")));

				// tap action click on the out of delivery area button as soon as the out of delivery area button is visible	
    			// Tap action by click on the date filed button as soon as the date filed button is visible
    			TouchAction t4 = new TouchAction(driver);
    			t4.tap(tapOptions().withElement(element(tenthOPtion))).perform();
    			
    			Thread.sleep(6000);
    			// click on mobile back icon
    			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
    			
    			Thread.sleep(2000);
    			// click on mobile back icon
    			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
	            		


		

	}
	
}
		
	@Step("Click on any open Conversation")
	public void clickOnanyOpenConversationFiled(String index) throws Exception {
		
		//click on openchat filed
		clickonOPenChatFiled();

		switch (index) {

		case "1":

			WebElement date = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/androidx.appcompat.widget.LinearLayoutCompat/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]"));

			// explicit wait - to wait for the First filed to be click-able
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/androidx.appcompat.widget.LinearLayoutCompat/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]")));

			// click on the any First filed as soon as the first filed is visible
			date.click();

			break;

		case "2":

			WebElement date2 = driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/androidx.appcompat.widget.LinearLayoutCompat/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]"));

			// explicit wait - to wait for the second filed  to be click-able
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/androidx.appcompat.widget.LinearLayoutCompat/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["
							+ index + "]")));

			// click on the second filed as soon as the second filed is visible
			date2.click();

			break;

		default:

			System.out.println("There is no any Option is displaying");

		}

	}
	

}
