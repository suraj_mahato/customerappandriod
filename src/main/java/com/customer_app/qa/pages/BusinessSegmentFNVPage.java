package com.customer_app.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.customer_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class BusinessSegmentFNVPage extends TestBase {

	// Page Factory OR:
	
	@FindBy(xpath = "//android.widget.TextView[@text='Products']")
	WebElement productsIconXpath;
	
	
	
	
	
	
	
	

	// Initializing the page objects
	public BusinessSegmentFNVPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("clicking on menu icon text ")
	public void clickOnMenuIcon() throws Exception {

		// explicit wait - to wait for the menu button to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Menu']")));


	}

}
