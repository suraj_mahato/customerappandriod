package com.customer_app.qa.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;

public class PlaceOrderForNextDayPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath ="//android.widget.FrameLayout[@content-desc=\"Menu\"]/android.widget.ImageView")
	WebElement menuIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Support']")
	WebElement supportlinkXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Getting started']")
	WebElement getStartedXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Place an order']")
	WebElement placeanOrdersectionXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Place an order for next day?']")
	WebElement placeanOrderForNextDayButtonpath;
	
	
	String listXpath="//android.widget.ListView[@index='0']/android.widget.RelativeLayout";
	
	
	

	// Initializing the page objects
	public PlaceOrderForNextDayPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("click on menu icon text")
	public void clickOnMenuIcon() throws Exception {

		menuIconXpath.click();

	}
	

	@Step(" click on Support Section")
    public void clickOnSupportButton() throws Exception {
		
		
		MobileElement el = (MobileElement) driver
			    .findElementByAndroidUIAutomator("new UiScrollable("
			        + "new UiSelector().scrollable(true)).scrollIntoView("                      
			        + "new UiSelector().textContains(\"Support\"));");
		 
		//click on support Button
		supportlinkXpath.click();
	
		
	}
	
	@Step("click on Getting strted text")
	public void clickOnGettingStartedText() throws Exception {

		getStartedXpath.click();

	}
	
	@Step("click on place an order text")
	public void clickOnPlaceanOrderSection() throws Exception {

		placeanOrdersectionXpath.click();

	}
	
	@Step("click on place an order for next day button")
	public void clickOnPlaceanOrderForNextDayButton() throws Exception {

		placeanOrderForNextDayButtonpath.click();

	}
	
	@Step("Fetching all product name ")
	public void titleOfProduct() throws Exception {

		//creating a list for all products
		List<MobileElement> listelements = driver.findElements(By.xpath(listXpath));

		//print a count of products list
		System.out.println("list count: " + listelements.size());

		
		for (int i = 0; i <listelements.size(); i++) {

			//product wise section xpath
			String titleXpath = "//android.widget.ListView[@index='0']/android.widget.RelativeLayout[@index=" + i
					+ "]/android.widget.TextView[1]";

			
			List<MobileElement> elementlist = driver.findElements(By.xpath(titleXpath));

			for (int j = 0; j < elementlist.size(); j++) {

				//xpath for title name
				String nameTextXpath = "//android.widget.ListView[@index='0']/android.widget.RelativeLayout[@index=" + i
						+ "]/android.widget.TextView[@index='1']";

				//xpath for MRP 
				String mrpXpath = "//android.widget.ListView[@index='0']/android.widget.RelativeLayout[@index="
						+ i + "]/android.widget.TextView[@index='2']";


				WebElement nameText = driver.findElement(By.xpath(nameTextXpath));

				WebElement mrpText = driver.findElement(By.xpath(mrpXpath));
	

				//printing 	product Name
				System.out.println("Product Name: " +nameText.getText().toString());

				//printing 	product Mrp
				System.out.println( "Product MRP: " +mrpText.getText().toString());
	

			}

			System.out.println("=========================================");

		}
		
		
		
		
	}
	
	@Step("Fetching all product name ")
	public void titleOfProduct1() throws Exception {


		//Fetch All the Products count text
		List<MobileElement> listelements = driver.findElements(By.xpath(listXpath));

		// print a count of products list
		System.out.println("list count: " + listelements.size());

		for (int i = 0; i < listelements.size(); i++) {

			// product wise section xpath
			String titleXpath = "//android.widget.ListView[@index='0']/android.widget.RelativeLayout[@index=" + i
					+ "]/android.widget.TextView[1]";


			//Fetch All the Products Text
			List<MobileElement> elementlist = driver.findElements(By.xpath(titleXpath));

			String product_name;
			String product_price;
			String nameTextXpath;
			String mrpXpath;

			// Use of HashMaop to store Products and Their price
			HashMap<String, String> map_final_products = new HashMap<String, String>();

			for (int j = 0; j < elementlist.size(); j++) {

				// xpath for title name
				nameTextXpath = "//android.widget.ListView[@index='0']/android.widget.RelativeLayout[@index=" + i
						+ "]/android.widget.TextView[@index='1']";

				// xpath for MRP
				mrpXpath = "//android.widget.ListView[@index='0']/android.widget.RelativeLayout[@index=" + i
						+ "]/android.widget.TextView[@index='2']";

				WebElement nameText = driver.findElement(By.xpath(nameTextXpath));

				WebElement mrpText = driver.findElement(By.xpath(mrpXpath));

				product_name = nameText.getText();// Iterate and fetch product name
				product_price = mrpText.getText();// Iterate and fetch product price
				
				map_final_products.put(product_price, product_name); //Add product and price in HashMap

				System.out.println("Product Name and price fetched from Andriod Device and saved in HashMap as:"
						+ map_final_products.toString());

				
				
			}

			System.out.println("=========================================");

		}

	}
	
	
	
}
