package com.customer_app.qa.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.customer_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Step;

public class ProfilePage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.FrameLayout[@content-desc=\"Menu\"]/android.widget.ImageView")
	WebElement menuXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='My Profile']")
	WebElement MyProfileLinkXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_edit_basic']")
	WebElement ProfileEditIconXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_edit_address']")
	WebElement editAddressIconXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_edit_preference']")
	WebElement editDeliveryPreferenceIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='DONE']")
	WebElement doneXpath;

	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/etFlat']")
	WebElement name_Xpath;
	
	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_name']")
	WebElement NewUsername_Xpath;

	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/etLocality']")
	WebElement email_Xpath;

	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/etArea']")
	WebElement alternateNo_Xpath;

	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_flat']")
	WebElement flatNo_Xpath;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText[2]")
	WebElement locality_Xpath;

	@FindBy(id = "app.mycountrydelight.in.countrydelight:id/etLandmark")
	WebElement landmark_Xpath;

	@FindBy(xpath = "//android.widget.Button[@text='SKIP']")
	WebElement skipXpath;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement navigateXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Calendar']")
	WebElement calenderIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Profile']")
	WebElement validateMessageXpath;

	@FindBy(xpath = "//android.widget.Button[@resource-id='android:id/button2']")
	WebElement OkXpath;

	@FindBy(xpath = "//android.widget.Button[@text='ADD FLAT NO / HOUSE NO / BUILDING']")
	WebElement AddFlatNoButtonXpath;

	@FindBy(xpath = "//android.widget.Button[@resource-id='app.mycountrydelight.in.countrydelight:id/btn_save']")
	WebElement SaveAddressButtonXpath;

	@FindBy(xpath = "//android.widget.EditText[@resource-id='app.mycountrydelight.in.countrydelight:id/et_locality']")
	WebElement DeliveryLocationFiled_Xpath;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='app.mycountrydelight.in.countrydelight:id/tv_change']")
	WebElement ChnageLinkXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_bell']")
	WebElement RingEditIconXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_slot']")
	WebElement TimeSlotEditIconXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_voice']")
	WebElement VoiceEditIconXpath;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_play']")
	WebElement PlayIconXpath;

	@FindBy(xpath = "//android.widget.Button[@resource-id='android:id/button1']")
	WebElement GpsOkXpath;

	@FindBy(xpath = "//android.widget.Button[@resource-id='app.mycountrydelight.in.countrydelight:id/btn_no']")
	WebElement DonotRingButtonXpath;

	@FindBy(xpath = "//android.widget.Button[@resource-id='app.mycountrydelight.in.countrydelight:id/btn_yes']")
	WebElement RingBellButtonXpath;

	@FindBy(xpath = "//android.widget.RadioButton[@text='5.00 AM - 7.30 AM']")
	WebElement selectMoringXpath;

	@FindBy(xpath = "//android.widget.RadioButton[@text='5.00 PM - 7.30 PM']")
	WebElement selectEveningXpath;

	@FindBy(xpath = "//android.widget.Button[@resource-id='app.mycountrydelight.in.countrydelight:id/btn_record']")
	WebElement RecordButtonXpath;

	@FindBy(xpath = "//android.widget.Button[@text='STOP']")
	WebElement StopButtonXpath;

	@FindBy(xpath = "//android.widget.Button[@text='SAVE']")
	WebElement SaveButtonXpath;

	@FindBy(xpath = "//android.widget.Button[@text='RETRY']")
	WebElement RetryButtonXpath;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='app.mycountrydelight.in.countrydelight:id/menu_done']")
	WebElement DoneButtonXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Wallet']")
	WebElement WalletFiledXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Support']")
	WebElement SupportFiledXpath;	
	
	@FindBy(xpath = "//android.widget.TextView[@text='Application Guide']")
	WebElement ApplicationFiledXpath;		
	
	@FindBy(xpath = "//android.widget.TextView[@text='Place an order']")
	WebElement PlaceOrderFiledXpath;	
	
	@FindBy(xpath = "//android.widget.TextView[@text='Place an order for next day?']")
	WebElement PlaceOrderNextFiledXpath;	
	
	@FindBy(xpath = "//android.widget.TextView[@text='Products']")
	WebElement ProductTextXpath;	
	
	@FindBy(xpath = "//android.widget.TextView[@text='DAIRY PRODUCTS']")
	WebElement DairyProductTextXpath;		
	
	@FindBy(xpath = "//android.widget.TextView[@text='Cow Milk 500 ML']")
	WebElement CowMilkFiledXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='TestMilk']")
	WebElement TestMilkFiledXpath;	
	
	@FindBy(xpath = "//android.widget.TextView[@text='ADD']")
	WebElement CowMilkPlusIconXpath;
	
	@FindBy(xpath = "//android.widget.Button[@text='+']")
	WebElement TestMilkPlusIconXpath;	
	
	@FindBy(xpath = "//android.widget.Button[@text='Update']")
	WebElement UpdateButton;
	
	@FindBy(xpath="//android.widget.TextView[@text='Confirm']")
	WebElement confirmButtonXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='Skip preferences for now']")
	WebElement SkipPreferenceForNowLinkXpath;	
	

	// Initializing the page objects
	public ProfilePage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("Validate that validation message ")
	public String validateValidationMessage() throws Exception {

		return validateMessageXpath.getText();

	}

	@Step("Verify click on more icon ")
	public ProfilePage MenuLink() throws Exception {

		// Click on more icon text

		menuXpath.click();
		return new ProfilePage();

	}

	@Step("Verify click on skip popup ")
	public ProfilePage skipPopup() throws Exception {

		// Click on more icon text

		skipXpath.click();
		return new ProfilePage();

	}

	@Step("Verify click on calender icon ")
	public ProfilePage calenderText() throws Exception {

		// Click on calender icon

		calenderIconXpath.click();
		return new ProfilePage();

	}

	@Step("Verify click on navigate back")
	public ProfilePage navigateBack() throws Exception {

		// Click on navigate Back

		navigateXpath.click();

		return new ProfilePage();

	}

	@Step("Verify click on profile text")
	public ProfilePage profileLink() throws Exception {

		// Click on profile text

		MyProfileLinkXpath.click();
		return new ProfilePage();

	}

	@Step("Verify click on profile text")
	public ProfilePage editlink() throws Exception {

		// Click on profile edit icon link

		ProfileEditIconXpath.click();
		return new ProfilePage();

	}

	@Step("Verify click on address edit icon")
	public void editAddresslink() throws Exception {

		// click on address edit icon
		editAddressIconXpath.click();

		// checking validation message is display or not

		if (!driver.findElements(By.xpath("//android.widget.Button[@resource-id='android:id/button1']")).isEmpty()) {

			System.out.println("Popup is display");

			// click on ok button
			GpsOkXpath.click();

		} else {

			System.out.println("When popup is not display");

		}

	}
	
	@Step("Click on mobile OS Back button")
	public void ClickOnMobileOSBackButton() throws Exception {

		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		// checking validation message is display or not

		if (!driver.findElements(By.xpath("//android.widget.Button[@resource-id='android:id/button1']")).isEmpty()) {

			System.out.println("Popup is display");

			// click on save button
			SaveButtonXpath.click();
			

		} else {

			System.out.println("When popup is not display");

		}

	}
	
	
	@Step("Click on skip preference for now link " )
	public void ClickOnSkipPreferenceForNowLink() throws Exception {
		
		//Click on skip preference for now link
		SkipPreferenceForNowLinkXpath.click();
		
		
	}
	
	@Step("Click on back Icon")
	public void ClickOnBackIcon() throws Exception {

		// click on back icon
		navigateXpath.click();

		// checking validation message is display or not

		if (!driver.findElements(By.xpath("//android.widget.Button[@resource-id='android:id/button1']")).isEmpty()) {

			System.out.println("Popup is display");

			// click on save button
			SaveButtonXpath.click();
			

		} else {

			System.out.println("When popup is not display");

		}

	}

	@Step("Verify click on  Save Address Button")
	public void ClickOnSaveAddressButton() throws Exception {

		// click on Save Address Button

		SaveAddressButtonXpath.click();

	}

	@Step("Verify click on delivery preference edit icon")
	public ProfilePage DeliveryPreferenceEditIcon() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();
		return new ProfilePage();

	}

	@Step("click on donot select ring button")
	public void DonotSelectRingBellRadioButton() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();

		Thread.sleep(2000);
		// click on ring the bell edit icon
		RingEditIconXpath.click();

		// click on donot select ring bell button
		DonotRingButtonXpath.click();

	}

	@Step("click on select ring the bell button")
	public void SelectRingBellRadioButton() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();

		Thread.sleep(2000);
		// click on ring the bell edit icon
		RingEditIconXpath.click();

		// click on select ring bell button
		RingBellButtonXpath.click();

	}

	@Step("click on moring timeslot button")
	public void SelectMoringTimeSlotButton() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();

		// checking validation message is display or not

		if (!driver.findElements(By
				.xpath("//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_slot']"))
				.isEmpty()) {

			System.out.println("TimeSlot filed edit icon is display");

			Thread.sleep(2000);
			// click on timeslot edit icon
			TimeSlotEditIconXpath.click();

			Thread.sleep(2000);
			// click on morining timeslot button
			selectMoringXpath.click();

		} else {

			System.out.println("TimeSlot filed edit icon is not display");

		}

	}

	@Step("click on evening timeslot button")
	public void SelectEveningTimeSlotButton() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();

		if (!driver.findElements(By
				.xpath("//android.widget.ImageView[@resource-id='app.mycountrydelight.in.countrydelight:id/img_slot']"))
				.isEmpty()) {

			System.out.println("TimeSlot filed edit icon is display");

			Thread.sleep(2000);
			// click on timeslot edit icon
			TimeSlotEditIconXpath.click();

			// click on evening timeslot button
			selectEveningXpath.click();

		} else {

			System.out.println("TimeSlot filed edit icon is not display");

		}

	}

	@Step("Adding voice record")
	public void AddingVoice() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();

		Thread.sleep(2000);
		// click on voice edit icon
		VoiceEditIconXpath.click();

		// click on record button
		RecordButtonXpath.click();

		// click on stop button
		Thread.sleep(5000);
		StopButtonXpath.click();

		// click on save button
		SaveButtonXpath.click();

	}

	@Step("Updating the voice record")
	public void RetryVoice() throws Exception {

		// click on delivery preference edit icon
		editDeliveryPreferenceIconXpath.click();

		Thread.sleep(2000);
		// click on voice edit icon
		VoiceEditIconXpath.click();

		// click on record button
		RecordButtonXpath.click();

		// click on stop button
		Thread.sleep(5000);
		StopButtonXpath.click();

		// click on retry button
		RetryButtonXpath.click();

		// click on record button
		RecordButtonXpath.click();

		// click on stop button
		Thread.sleep(5000);
		StopButtonXpath.click();

		Thread.sleep(2000);
		// click on save button
		SaveButtonXpath.click();

	}

	@Step("Click on done button")
	public void doneButton() throws Exception {

		// Click on done button
		doneXpath.click();

		// when validation message is display or not

		if (!driver.findElements(By.xpath(
				"//android.widget.TextView[@resource-id='app.mycountrydelight.in.countrydelight:id/snackbar_text']"))
				.isEmpty()) {

			System.out.println("Popup is display");

			// click on mobile back icon
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		} else if (!driver.findElements(By.xpath("//android.widget.Button[@resource-id='android:id/button2']"))
				.isEmpty()) {

			System.out.println("Getting profile failed Popup is display");

			// click on mobile back icon
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

			Thread.sleep(2000);
			// click on mobile back icon
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		} else {

			System.out.println("When popup is not display");

		}

	}

	@Step("Click on delivery preference filed done button")
	public void DeliveryPreferenceDoneButton() throws Exception {

		// Done button text is enabled or not ?

		if (driver.findElement(By
				.xpath("//android.widget.TextView[@resource-id='app.mycountrydelight.in.countrydelight:id/menu_done']"))
				.isEnabled()) {

			System.out.println("Done Button is Enabled");

			// Click on done button
			DoneButtonXpath.click();

		} else {

			System.out.println("Done Button is not display");

			// click on mobile back icon
			Thread.sleep(2000);
			driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		}

	}

	
	@Step("New User first click on wallet then it is navigate to profile screen")
	public void ClickonWallet() throws Exception {
		
		//click on wallet field
		WalletFiledXpath.click();
		 
	}
	
	
	@Step("New User first click on products link and select the products(Dairy), update the qty and click on confirm button then it is navigate to profile screen")
	public void ClickonProducts() throws Exception {
		
		Thread.sleep(3000);
		//click on products link
		ProductTextXpath.click();
		
		//click on dairy product
		DairyProductTextXpath.click();
		
		//select the cow milk

		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"Cow Milk 500 ML\"));");
	
		//click on cow milk filed
	    CowMilkFiledXpath.click();
	    
	    //update the qty of cow milk
	    CowMilkPlusIconXpath.click();
	    
	    //click on confirm button
	    confirmButtonXpath.click();
	   
		 
	}	
	
	@Step("New User first move on menu filed --> click on support filed --> place an order for subscription --> select the products(Dairy), update the qty and click on confirm button then it is navigate to profile screen")
	public void ClickOnSupportFirstSelectTheProduct() throws Exception {
		
		
		MobileElement els = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"Support\"));");
	
	 	//click on support filed
	    SupportFiledXpath.click();
	    
	    //click on application guide
	    ApplicationFiledXpath.click();
	    
	    //click on place an order button
	    PlaceOrderFiledXpath.click();
	    
	    //click on place an order for next day
	    PlaceOrderNextFiledXpath.click();
		
		//click on dairy product
		DairyProductTextXpath.click();
		
		//select the cow milk

		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator("new UiScrollable("
				+ "new UiSelector().scrollable(true)).scrollIntoView(" + "new UiSelector().textContains(\"TestMilk\"));");
	    
	    //update the qty of test milk
	    TestMilkPlusIconXpath.click();
	    
	    //click on updated button
	    UpdateButton.click();
	   
		 
	}	
	
	
	
	
	@Step("Verify that Edit Profile with Name: {0} , Email: {1} and Alt_Mobile: {2} ")
	public ProfilePage profileDetail(String Name, String Email, String AltNumber) throws Exception {

		// click on name filed
		name_Xpath.click();

		// clear the previous text
		name_Xpath.clear();

		// enter name
		Actions a = new Actions(driver);
		a.sendKeys(Name);
		a.perform();

		// click on email filed
		email_Xpath.click();

		// clear the previous text
		email_Xpath.clear();

		// enter email id
		Actions e = new Actions(driver);
		e.sendKeys(Email);
		e.perform();

		// click on alternate no filed
		alternateNo_Xpath.click();

		// clear the previous text
		alternateNo_Xpath.clear();

		// enter alternate no.
		Actions f = new Actions(driver);
		f.sendKeys(AltNumber);
		f.perform();

		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		return new ProfilePage();

	}

	@Step("Verify that Edit Address with FlatNo: {0} , Delivery location: {1} ")
	public ProfilePage AdressDetails(String flatNo, String DLocation) throws Exception {

		// click on add flatNo button
		AddFlatNoButtonXpath.click();

		// click on flat no filed
		flatNo_Xpath.click();

		// clear the previous text
		flatNo_Xpath.clear();

		// enter flatNo
		Actions a = new Actions(driver);
		a.sendKeys(flatNo);
		a.perform();

		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		// click on delivery location filed
		DeliveryLocationFiled_Xpath.click();

		// clear the previous text
		DeliveryLocationFiled_Xpath.clear();

		// enter email id
		Actions e = new Actions(driver);
		e.sendKeys(DLocation);
		e.perform();

		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		return new ProfilePage();

	}
	
	@Step("Verify that Edit Profile with Name: {0} , flatNo: {1} and Delivery Location: {2} ")
	public void NewUserprofileDetail(String Name, String flatNo, String DLocation) throws Exception {

		// click on add flatNo button
		AddFlatNoButtonXpath.click();
		
		// click on name filed
		NewUsername_Xpath.click();

		// enter name
		Actions a = new Actions(driver);
		a.sendKeys(Name);
		a.perform();

		// click on flat no filed
		flatNo_Xpath.click();

		// clear the previous text
		flatNo_Xpath.clear();

		// enter flatNo
		Actions f = new Actions(driver);
		f.sendKeys(flatNo);
		f.perform();

		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));

		// click on delivery location filed
		DeliveryLocationFiled_Xpath.click();

		// clear the previous text
		DeliveryLocationFiled_Xpath.clear();

		// enter email id
		Actions e = new Actions(driver);
		e.sendKeys(DLocation);
		e.perform();

		// click on mobile back icon
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
	
		
	}


	@Step("Verify Customer Details with Name, EmailId and Alternate No: ")
	public ProfilePage customerProfileDetails(List<String> elements) throws Exception {

		// Array list for customer Details
		List<WebElement> xpaths = new ArrayList();

		// Name section Xpath
		xpaths.add(name_Xpath);

		// Phone Number section xpath
		xpaths.add(email_Xpath);

		// Alternate Phone Number section xpath
		xpaths.add(alternateNo_Xpath);

		// creating Actions class
		Actions a = new Actions(driver);
		int i = 0;

		// creating for loop for Customer Details
		for (WebElement element : xpaths) {

			// clicking on element object
			element.click();
			element.clear();

			String eleString = elements.get(i);
			a.sendKeys(eleString);
			a.sendKeys(Keys.ENTER);
			a.perform();
			i += 1;

			driver.hideKeyboard();

		}

		driver.hideKeyboard();

		return new ProfilePage();

	}

	@Step("Verify Customer Details with Name: {0} ,  ")
	public ProfilePage customerAddressDetails(List<String> elementss) throws Exception {

		// Array list for customer Details
		List<WebElement> xpaths = new ArrayList();

		// flatNo section xpath
		xpaths.add(flatNo_Xpath);

		// locality section xpath
		xpaths.add(locality_Xpath);

		// landmark Number section xpath
		xpaths.add(landmark_Xpath);

		// creating Actions class
		Actions a = new Actions(driver);
		int i = 0;

		// creating for loop for Customer Details
		for (WebElement element : xpaths) {

			// clicking on element object

			element.click();

			Thread.sleep(2000);

			element.clear();

			System.out.println("passed");

			String eleString = elementss.get(i);
			a.sendKeys(eleString);
			a.sendKeys(Keys.ENTER);
			a.perform();
			i += 1;

			driver.hideKeyboard();

		}

		driver.hideKeyboard();

		return new ProfilePage();

	}

	@Step("Select randomly any City and Locality from dropdown list")
	public void validateselectinganyCityLocality() throws Exception {

		// click on change link
		ChnageLinkXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath(
				"//androidx.recyclerview.widget.RecyclerView[@index='0']/android.view.ViewGroup/android.widget.TextView[@index='1']"));

//		System.out.println(elements.size());

		log.debug("Printing size:" + elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any city with any index wise
		elements.get(rndInt).click();

		Thread.sleep(2000);

		// checking wheather select city screen is displayed or not?

		if (!driver.findElements(By.xpath("//android.widget.TextView[@text='Select City']")).isEmpty()) {

			System.out.print(" List of select city is display");

			// list methods
			List<WebElement> selectCity = driver.findElements(By.xpath(
					"//androidx.recyclerview.widget.RecyclerView[@index='1']/android.view.ViewGroup/android.widget.TextView[@index='0']"));

			System.out.println(selectCity.size());

			Random rndno = new Random();

			int rndnoInt = rndno.nextInt(selectCity.size());

			System.out.println("Random Number : " + rndnoInt);

			// select randomly any city with any index wise
			selectCity.get(rndnoInt).click();

			Thread.sleep(3000);

			// list methods
			List<WebElement> selectLocality = driver.findElements(By.xpath(
					"//androidx.recyclerview.widget.RecyclerView[@index='1']/android.view.ViewGroup/android.widget.TextView[@index='0']"));

			System.out.println(selectLocality.size());

			Random rndno1 = new Random();

			int rndnoInt1 = rndno1.nextInt(selectLocality.size());

			System.out.println("Random Number : " + rndnoInt1);

			// select randomly any locality with any index wise
			selectLocality.get(rndnoInt1).click();

		} else {

			System.out.println(" List of select city is not is display");

			// list methods
			List<WebElement> selectLocality = driver.findElements(By.xpath(
					"//androidx.recyclerview.widget.RecyclerView[@index='1']/android.view.ViewGroup/android.widget.TextView[@index='0']"));

			System.out.println(selectLocality.size());

			Random rndno1 = new Random();

			int rndnoInt1 = rndno1.nextInt(selectLocality.size());

			System.out.println("Random Number : " + rndnoInt1);

			// select randomly any city with any index wise
			selectLocality.get(rndnoInt1).click();

		}
		

	}

}
