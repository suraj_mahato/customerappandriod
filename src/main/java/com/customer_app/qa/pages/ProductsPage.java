package com.customer_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.customer_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class ProductsPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@text='Products']")
	WebElement productsTextXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='FRUITS AND VEGETABLES']")
	WebElement fruiltsAndVegetablesXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='DAIRY PRODUCTS']")
	WebElement dailyProductsXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Products']")
	WebElement productsPageTextXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Create a Basket']")
	WebElement createBasketPageTextXpath;
	
	

	// Initializing the page objects
	public ProductsPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("click on products text icon")
	public void productsLink() throws Exception {

	    productsTextXpath.click();
	
	}
	
	public String verifyProductsPage() throws Exception {
		
	   return productsPageTextXpath.getText();

	}
	

}
