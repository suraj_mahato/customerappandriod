package com.customer_app.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class ExistingCustomerPage extends TestBase {

	
	//Page Factory OR:

	
		@FindBy(xpath="//android.widget.TextView[@text='Existing Customer']")
		WebElement existingCustomerXpath;
		
		@FindBy(id="app.mycountrydelight.in.countrydelight:id/phone_number_edittext")
		WebElement mobileNumberXpath;
		
		@FindBy(xpath="//android.widget.Button['Send OTP']")
		WebElement sendOTPXpath;
		
		@FindBy(xpath="//android.widget.ImageButton['Navigate up']")
		WebElement navigateBackpath;
		
		@FindBy(id="app.mycountrydelight.in.countrydelight:id/pin_edit_text")
		WebElement otpTextXpath;
		
		@FindBy(xpath="//android.widget.TextView[@text='Skip this']")
		WebElement skipLinkXpath;
		
		@FindBy(xpath="//android.widget.ImageButton[@content-desc='Navigate up']")
		WebElement drawerIconXpath;
		
		@FindBy(xpath="//android.widget.TextView[@text='Logout']")
		WebElement logOutXpath;
		
		@FindBy(xpath="//android.widget.Button[@text='YES']")
		WebElement popupYesXpath;
		
		@FindBy(xpath="//android.widget.Button[@text='CANCEL']")
		WebElement popupCancelXpath;
		
		@FindBy(id="app.mycountrydelight.in.countrydelight:id/resend_textview")
		WebElement resendXpath;
		
		@FindBy(xpath="//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView")
		WebElement moreXpath;
		
		
		

		// Initializing the page objects
		public ExistingCustomerPage() {
			PageFactory.initElements(driver, this);
		}

		
		//Actions
		
		@Step("Verify click on existing button")
		public ExistingCustomerPage existingCustomer() throws Exception {
		   	
			//Click on Existing Customer
		   	existingCustomerXpath.click();
			return new ExistingCustomerPage();
			
		}
		

		@Step("Verify click on more icon")
		public ExistingCustomerPage moreLink() throws Exception {
		   	
			//Click on more link
			moreXpath.click(); 
		    return new ExistingCustomerPage();
			
		}
		

		@Step("click on Logout button")
		public ExistingCustomerPage logoutText() throws Exception {
		   	
			//Click on logout link
			logOutXpath.click();
		    return new ExistingCustomerPage();
			
		}
		
		@Step("click on Logout popup Yes button button")
		public ExistingCustomerPage yesPopupButton() throws Exception {
		   	
			//Click on Yes Button
			popupYesXpath.click();
		    return new ExistingCustomerPage();
			
		}
		
		@Step("click on Logout popup cancel button")
		public ExistingCustomerPage cancelPopupButton() throws Exception {
		   	
			//Click on cancel Button
			popupCancelXpath.click();
		    return new ExistingCustomerPage();
			
		}
		
		
		@Step("Verify Mobile Number and OTP with Mobile Number: {0}, OTP: {1} " )
		public ExistingCustomerPage existingCustomerDetails(String Mobile, String Otp) throws Exception {
		
			//Click on Mobile Number section
			mobileNumberXpath.click();
			
			//Enter Mobile number
			Actions mobileSection = new Actions(driver);
			mobileSection.sendKeys(Mobile);
			mobileSection.perform();
			
		    //Click on Send Otp
			sendOTPXpath.click();
			
			//Click on Otp Section
			otpTextXpath.click();
			
			//Enter Otp Number
			Actions Otp_Section = new Actions(driver);
			Otp_Section.sendKeys(Otp);
			Otp_Section.perform();
			
			//Click on Skip link
			skipLinkXpath.click();
			
			return new ExistingCustomerPage();
			
		}
		
		@Step("Verify Mobile Number and OTP with Mobile Number: {0}, OTP: {1} " )
		public ExistingCustomerPage existingCustomerResend(String Mobile, String Otp, String newOtp) throws Exception {
		
			String correctOtp="12468";
			
			//Click on Mobile Number section
			mobileNumberXpath.click();
			
			//Enter Mobile number
			Actions mobileSection = new Actions(driver);
			mobileSection.sendKeys(Mobile);
			mobileSection.perform();
			
		    //Click on Send Otp
			sendOTPXpath.click();          

			otpTextXpath.click();
				
		    Actions Otp_Sections = new Actions(driver);
			Otp_Sections.sendKeys(Otp);
			Otp_Sections.perform();
			
			driver.hideKeyboard();
		
			if(Otp.equals(correctOtp)) {	
				
				//Click on Skip link
				skipLinkXpath.click();
				
				System.out.println("Verified Otp");
				
			}else {
				
				resendXpath.click();
				otpTextXpath.click();
				
				Actions Otp_Section = new Actions(driver);
				Otp_Section.sendKeys(newOtp);
				Otp_Section.perform();	
				
				//Click on Skip link
				skipLinkXpath.click();
				
				
			}	
			
			return new ExistingCustomerPage();
			
		}
		
		
		@Step("Verify Mobile Number with Mobile Number: {0} ")
		public ExistingCustomerPage enterMobileNumber(String Mobile) throws Exception {
		
			//Click on Mobile Number section
			mobileNumberXpath.click();
			
			//Enter Mobile number
			Actions mobileSection = new Actions(driver);
			mobileSection.sendKeys(Mobile);
			mobileSection.perform();
			
		    //Click on Send Otp
			sendOTPXpath.click();
			
			return new ExistingCustomerPage();
			
		}
	

		@Step("Click on mobile number")
		public void mobileNumber() throws Exception {
			
			
			mobileNumberXpath.click();
		
		}
		

		@Step("Click on navigate back icon")
		public void navigateBack() throws Exception {
		
		   navigateBackpath.click();
		
		}
		
	
		
}