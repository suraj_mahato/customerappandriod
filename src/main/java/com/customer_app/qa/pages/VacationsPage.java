package com.customer_app.qa.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;

public class VacationsPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@text='Menu']")
	WebElement menuIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Vacations']")
	WebElement vacationTextXpath;

	@FindBy(xpath = "//android.widget.Button[@text='Add Vacation']")
	WebElement addVacationXpath;

	@FindBy(xpath = "//android.widget.TextView[@content-desc=\"Add Vacation\"]")
	WebElement plusIconVacationXpath;

	@FindBy(xpath="//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement leftArrowXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Home']")
	WebElement homeIconXpath;
	
	@FindBy(xpath = "//android.widget.Button[@text='Optional']")
	WebElement optionalSectionXpath;
	
	@FindBy(xpath = "//android.widget.RadioButton[@text='Don't Choose Date']")
	WebElement donotChooseRadioButtonXpath;
	
	@FindBy(xpath = "//android.widget.Button[@text='OK']")
	WebElement okXpath;
	
	@FindBy(xpath="//android.view.View[@content-desc=\"25 June 2020\"]")
	WebElement  juneParticularDateXpath;

	@FindBy(xpath = "//android.widget.Button[@text='End Vacation']")
	WebElement endVacationLinkpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout/android.widget.LinearLayout")
	WebElement alreadyVacationXpath;
	
	
	
	// Initializing the page objects
	public VacationsPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("clicking on menu icon text ")
	public VacationsPage clickOnMenuIcon() throws Exception {

		menuIconXpath.click();

		return new VacationsPage();
	}
	
	@Step("clicking on vacationstext ")
	public VacationsPage clickOnVacationText() throws Exception {

         vacationTextXpath.click();

		return new VacationsPage();
	}
	

	@Step("clicking on Add Vacations Button ")
	public VacationsPage clickOnAddVacationButton() throws Exception {

         addVacationXpath.click();

		return new VacationsPage();
	
	}
	
	@Step("clicking on plus icon Vacations Button ")
	public VacationsPage clickOnPlusIconButton() throws Exception {

		plusIconVacationXpath.click();

		return new VacationsPage();
	
	}
	

	@Step("clicking on left arrow icon Button ")
	public VacationsPage clickOnLeftArrowIconButton() throws Exception {

		leftArrowXpath.click();

		return new VacationsPage();
	
	}
	
	@Step("clicking on home icon text ")
	public VacationsPage clickOnHomeIcon() throws Exception {

		homeIconXpath.click();

		return new VacationsPage();
	
	}
	
	@Step("clicking on optional section ")
	public VacationsPage clickOnOptionalSection() throws Exception {

		optionalSectionXpath.click();

		return new VacationsPage();
	
	}
	
	@Step("clicking on ok button ")
	public VacationsPage clickOnOkButton() throws Exception {

		okXpath.click();

		return new VacationsPage();
	
	}

	@Step("clicking on partcular date text ")
	public VacationsPage clickOnParticularDate() throws Exception {

		juneParticularDateXpath.click();
		Thread.sleep(2000);
		okXpath.click();	

		return new VacationsPage();
	
	}
	
	@Step("clicking on end vacation link text ")
	public VacationsPage clickOnEndVacationLink() throws Exception {

		endVacationLinkpath.click();

		return new VacationsPage();
	
	}
	
	
	@Step("clicking on already vacation text ")
	public VacationsPage clickOnalredayVacationSectionText() throws Exception {

		alreadyVacationXpath.click();

		return new VacationsPage();
	
	}


	@Step("clicking on randomly on particular date ")
	public VacationsPage clickOnparticulardateRandomly() throws Exception {

		
		String datePart="15 June 2020";
		
	/*	
		List<MobileElement> elements = driver.findElements(
				By.xpath("//android.view.View[@resource-id=‘android:id/month_view’]/android.view.View[@content-desc=’’ +\n" + 
						"datePart) + ‘’]’)"));

		System.out.println(elements.size());
		
		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size() - 1);

		// select randomly any city with any index
		elements.get(rndInt).click();
*/
		
		WebElement e= driver.findElement(By.xpath("//android.view.View[@resource-id=‘android:id/month_view’]/android.view.View[@content-desc=’’ +\n" + 
				"datePart) + ‘’]’)"));
		
		e.click();
		
		
		
		
		return new VacationsPage();
	
	}
	
	
}
