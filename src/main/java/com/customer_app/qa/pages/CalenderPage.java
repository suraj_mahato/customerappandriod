package com.customer_app.qa.pages;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer_app.qa.base.TestBase;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;

public class CalenderPage extends TestBase {

	
	//Page Factory OR:
	
	
	@FindBy(xpath="(//android.widget.ImageView[@content-desc=\"Juspay Logo\"])[2]")
	WebElement calenderRightArrowXpath;
	
	@FindBy(xpath="(//android.widget.ImageView[@content-desc=\"Juspay Logo\"])[1]")
	WebElement calenderLeftArrowXpath;
	
	@FindBy(id="app.mycountrydelight.in.countrydelight:id/tv_month")
	WebElement monthXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='Home']")
	WebElement homePageXpath;

	
	
	
	

	// Initializing the page objects
			public CalenderPage() {
				PageFactory.initElements(driver, this);
			}

			
			//Actions
			
			@Step("click on home icon text")
			public void clickOnHomeIcon() throws Exception {
			   	
				//Click on home iocn
				homePageXpath.click();
			  
				
				
			}
			
			@Step("Verify and swipe forword upto 3 Month")
			public void calenderSwipeForward() throws InterruptedException {

				Dimension dim = driver.manage().window().getSize();

				int height = dim.getHeight();
				int width = dim.getWidth();

				// For Swipe Left-Right
				int startx = (int) (width * .75);
				int endx = (int) (width * .10);
				int starty = height / 2;
				int endy = height / 2;

				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(startx, starty))
					  .moveTo(PointOption.point(endx, endy))
					  .release()
					  .perform();

			}

			@Step("Verify and swipe backward upto 3 Month")
			public void calenderSwipeBackward() throws InterruptedException {

				Dimension dim = driver.manage().window().getSize();

				int height = dim.getHeight();
				int width = dim.getWidth();

				// For Swipe Right-Left

				int startx = (int) (width * .75);
				int endx = (int) (width * .10);
				int starty = height / 2;
				int endy = height / 2;

				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(endx, endy))
						.moveTo(PointOption.point(startx, starty))
						.release()
						.perform();

			}

			
			@Step("Verify and swipe forword upto 3 Month")
			public void maincalenderEndMonthSwipe(String EndMonth) throws Exception {

				while (true) {

					String text = monthXpath.getText();

					if (text.equals(EndMonth)) {
						break;

					} else {

						calenderSwipeForward();
					}

				}

			}

			@Step("Verify and swipe backward upto 3 Month")
			public void maincalenderEndMonth(String EndMonth) throws Exception {

				while (true) {

					String text = monthXpath.getText();

					if (text.equals(EndMonth)) {

						break;

					} else {

						Thread.sleep(2000);
						calenderRightArrowXpath.click();
					}

				}

			}
			
			
			@Step("Verify and swipe forword upto 3 Month")
			public void maincalenderStartMonthSwipe(String StartMonth) throws Exception {

				while (true) {

					String text = monthXpath.getText();

					if (text.equals(StartMonth)) {

						break;

					} else {

						calenderSwipeBackward();

					}

				}
			}

			@Step("Verify that moving backward upto 3 Month")
			public void maincalenderStartMonth(String StartMonth) throws Exception {

				while (true) {

					String text = monthXpath.getText();

					if (text.equals(StartMonth)) {

						break;

					} else {

						Thread.sleep(2000);
						calenderLeftArrowXpath.click();

					}

				}

			}
	
}