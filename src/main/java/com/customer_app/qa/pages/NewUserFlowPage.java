package com.customer_app.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.customer_app.qa.base.TestBase;

import io.qameta.allure.Step;

public class NewUserFlowPage extends TestBase {

	// Page Factory OR:
	
	@FindBy(xpath = "//android.widget.TextView[@text='Menu']")
	WebElement menuIconXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Refer and Earn']")
	WebElement MenuInsideReferEarnFiledXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@resource-id ='app.mycountrydelight.in.countrydelight:id/tv_balance']")
	WebElement WalletIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Refer & Earn']")
	WebElement ReferEarnXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Products']")
	WebElement productXpath;	
	
	@FindBy(xpath= "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView")
	WebElement nextDayDateXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Add Products']")
	WebElement AddproductXpath;	
	
	@FindBy(xpath = "//android.widget.TextView[@text='Summer Specials']")
	WebElement SummerSpecialFiledXpath;
	
	@FindBy(xpath= "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[1]/android.widget.ImageView[2]")
	WebElement plusIconXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='ADD']")
	WebElement AddButtonXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Daily']")
	WebElement DailyButtonXpath;
	
    @FindBy(xpath= "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[31]")
    WebElement startDateXpath;
    
	@FindBy(xpath = "//android.widget.TextView[@text='Done']")
	WebElement DoneButtonXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Confirm']")
	WebElement ConfirmButtonXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Fruits']")
	WebElement FruiltsFiledXpath;
	
	@FindBy(xpath = "//android.widget.TextView[@resource-id ='app.mycountrydelight.in.countrydelight:id/cl_new_order']")
	WebElement CreateNewOrderFiledXpath;
	
	@FindBy(xpath= "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[31]")
	WebElement FNVStartDateXpath;
	
	@FindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView")
	WebElement FNVPlusXpath;
	
	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView[2]")
	WebElement plusXpath;
	
	@FindBy(xpath = "//android.widget.Button[@text='PROCEED']")
	WebElement ProceedButtonXpath;
	
	@FindBy(xpath = "//android.widget.Button[@resource-id ='android:id/button1']")
	WebElement OkXpath;
	
	
	

	// Initializing the page objects
	public NewUserFlowPage() {
		PageFactory.initElements(driver, this);
	
	}

	
	// Actions

	@Step("clicking on wallet icon filed ")
	public void clickOnWalletIconFiled() throws Exception {

		// explicit wait - to wait for the wallet icon to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@resource-id ='app.mycountrydelight.in.countrydelight:id/tv_balance']")));

		// click on the wallet icon as soon as the "Wallet" icon is visible
		WalletIconXpath.click();
	

	}
	

	@Step("clicking on Refer and earn icon ")
	public void clickOnReferEarnIcon() throws Exception {

		// explicit wait - to wait for the refer and earn icon to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Refer & Earn']")));

		// click on the refer and earn as soon as the "refer and earn" icon is visible
		ReferEarnXpath.click();
	

	}
	

	@Step("clicking on ok button ")
	public void clickOnOkButton() throws Exception {

		// explicit wait - to wait for the ok popup button to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@resource-id ='android:id/button1']")));

		// click on the ok popup button as soon as the "ok" popup button is visible
		OkXpath.click();
	

	}
	
	@Step("clicking on menu icon text ")
	public void clickOnMenuIcon() throws Exception {

		// explicit wait - to wait for the menu button to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Menu']")));

		// click on the menu button as soon as the "Menu" button is visible
		menuIconXpath.click();

	}
	
	@Step("clicking on Refer and earn filed ")
	public void clickOnReferEarnFiled() throws Exception {

		// explicit wait - to wait for the refer and earn filed to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Refer and Earn']")));

		// click on the refer and earn filed as soon as the refer and earn filed is visible
		MenuInsideReferEarnFiledXpath.click();

	}
	
	
	@Step("Select any dairy product ")
	public void SelectanyDairyProduct() throws Exception {

		// explicit wait - to wait for the products icon to be click-able
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Products']")));

		// click on the products icon as soon as the products icon is visible
		productXpath.click();
		
		// explicit wait - to wait for the summer specials  to be click-able
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Summer Specials']")));
		

		
	}
	
	
	
	
	


}
