package com.customer_app.qa.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer_app.qa.base.TestBase;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;

public class OrderTrailPage extends TestBase {

	
	//Page Factory OR:

	
	@FindBy(xpath="//android.widget.Button[@text='Order Trial']")
	WebElement orderTrailXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='START DATE']")
	WebElement startDateXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='PROCEED']")
	WebElement proceedXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='Pay after 5 days']")
	WebElement payLaterXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='Add an address']")
	WebElement deliveryAddressXpath;

	@FindBy(xpath="//android.widget.Button[@text='ADD FLAT NO / HOUSE NO / BUILDING']")
	WebElement addFlatNoXpath;
	
	@FindBy(xpath="//android.widget.EditText[@text='Name']")
	WebElement nameXpath;
	
	@FindBy(xpath="//android.widget.EditText[@text='Flat no/ House no/ Building']")
	WebElement flatNoXpath;
	
	@FindBy(xpath="//android.widget.Button[@text='SAVE ADDRESS']")
	WebElement saveAddressXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText[3]")
	WebElement areaCityXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView")
	WebElement buffaloMilkXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ImageView")
	WebElement cowMilkXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.ImageView")
	WebElement lowFatCowMilkXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.ImageView")
	WebElement gharJaisaDahiXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView")
    WebElement taazaPaneerXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.TextView")
	WebElement lowFatDahiXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView")
	WebElement proteinBrownXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[6]/android.view.ViewGroup/android.widget.TextView")
	WebElement proteinWhiteXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView")
	WebElement whiteEggXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
	WebElement brownBreadXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView")
	WebElement whiteBreadXpath;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.TextView")
	WebElement bagXpath;
	
	@FindBy(xpath="//android.widget.Button[@text='BACK TO PRODUCTS']")
	WebElement backtoProductXpath;
	
	@FindBy(id="app.mycountrydelight.in.countrydelight:id/tv_edit_address") 
	WebElement editXpath;
	
	@FindBy(xpath="//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement navigateBackXpath;
	
	
	
	
	// Initializing the page objects
			public OrderTrailPage() {
				PageFactory.initElements(driver, this);
			}

			
			//Actions
			
			@Step("Verify click on order trail button")
			public OrderTrailPage ordertrailTap() throws Exception {
			   	
				
				orderTrailXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			
			@Step("Verify click on edit link")
			public OrderTrailPage editLink() throws Exception {
			   	
				
				editXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			
			
			
			@Step("Verify click on Back to Products button")
			public OrderTrailPage backToProductsButton() throws Exception {
			   	
				
				backtoProductXpath.click();
			     	
				return new OrderTrailPage();
				
			}


			@Step("Verify click on left arrow icon")
			public OrderTrailPage clickOnLeftArrowIcon() throws Exception {
			   	
				
				navigateBackXpath.click();
			     	
				return new OrderTrailPage();
				
			}

			
			
			
			
			@Step("Verify click on buffalo milk qty with 1")
			public OrderTrailPage updateBuffaloMilk() throws Exception {
			   	
				
				buffaloMilkXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			
			
			
			@Step("Verify click on cow milk, ghar jaisa dahi and taaza paneer qty with 1")
			public OrderTrailPage updateMutipleProducts() throws Exception {
			   	
				
				cowMilkXpath.click();	
				Thread.sleep(2000);
				
				gharJaisaDahiXpath.click();
				Thread.sleep(2000);
				
				taazaPaneerXpath.click();
				Thread.sleep(2000);
			
				
				
				return new OrderTrailPage();
				
			}
			
			
			

			@Step("Verify click on start Date Button")
			public OrderTrailPage startDateButton() throws Exception {
			   	
				
				 startDateXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			

			@Step("Verify click on start Date Button")
			public OrderTrailPage proceedButton() throws Exception {
			   	
				
				 proceedXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			
			
			@Step("Verify click on payLater button")
			public OrderTrailPage payLaterButton() throws Exception {
			   	
				
				payLaterXpath.click();
			     	
				return new OrderTrailPage();
				
			}
	
	
			@Step("Verify click on delivery Address section")
			public OrderTrailPage deliveryAddress() throws Exception {
			   	
				
				deliveryAddressXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			

			@Step("Verify click on Add Flat No button")
			public OrderTrailPage addFlatNo() throws Exception {
			   	
				
				addFlatNoXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			

			@Step("Verify click on Add Flat No button")
			public OrderTrailPage areaCitySection() throws Exception {
			   	
				
				areaCityXpath.click();
				Thread.sleep(2000);
				
				areaCityXpath.click();
				
			     	
				return new OrderTrailPage();
				
			}
			
			
			
			@Step("Verify Customer Details with Name: {0} , Phone No: {1} , Alt No: {2} and Email Id: {3} ")
			public OrderTrailPage customerDetails(String Name, String FlatNo)
					throws Exception {

				nameXpath.click();
				Actions a = new Actions(driver);
				a.sendKeys(Name);
				a.sendKeys(Keys.ENTER);
				a.perform();
			
				flatNoXpath.click();
				Actions f = new Actions(driver);
				f.sendKeys(FlatNo);
				f.sendKeys(Keys.ENTER);
				f.perform();	
				
				driver.hideKeyboard();
				
				return new OrderTrailPage();
			
			}
			
			@Step("Verify click on save Address button")
			public OrderTrailPage saveAddress() throws Exception {
			   	
				
				saveAddressXpath.click();
			     	
				return new OrderTrailPage();
				
			}
			

			@Step("Random selecting City and Locality")
			public OrderTrailPage areaCity() {

				// click on Area/City section link
				areaCityXpath.click();

				try {
					
				
				// select random region
				List<MobileElement> elements = driver.findElements(
						By.xpath("//android.support.v7.widget.RecyclerView[@index='0']/android.view.ViewGroup"));
				
				System.out.println(elements.size());

				Random rnd = new Random();

				int rndInt = rnd.nextInt(elements.size());

				// select randomly any city with any index
				elements.get(rndInt).click();
		
			

				List<MobileElement> elements1 = driver.findElements(
						By.xpath("//android.support.v7.widget.RecyclerView[@index='1']/android.view.ViewGroup"));
				
				System.out.println(elements1.size());

				Random rnds = new Random();

				int rndsInt = rnds.nextInt(elements1.size());

				// select randomly any locality with any index
				elements1.get(rndsInt).click();
				
				}
				
				catch(Exception e) {
					

					List<MobileElement> elements1 = driver.findElements(
							By.xpath("//android.support.v7.widget.RecyclerView[@index='1']/android.view.ViewGroup"));
					
					System.out.println(elements1.size());

					Random rnds = new Random();

					int rndsInt = rnds.nextInt(elements1.size());

					// select randomly any locality with any index
					elements1.get(rndsInt).click();
					
				}
				
				return new OrderTrailPage();
			
			}

			
			
			
			
}
