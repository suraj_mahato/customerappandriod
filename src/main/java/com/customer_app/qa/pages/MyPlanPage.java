package com.customer_app.qa.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer_app.qa.base.TestBase;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;

public class MyPlanPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//android.widget.TextView[@text='ADD']")
	WebElement addPlusXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Confirm']")
	WebElement confirmXpath;

	@FindBy(xpath = "//android.widget.FrameLayout[@content-desc=\"My Plan\"]/android.view.ViewGroup/android.widget.TextView")
	WebElement myPlanXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='MY PLAN']")
	WebElement bannerMyPlanXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='ORDER NOW']")
	WebElement bannerOrderNowXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Menu']")
	WebElement menuIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Support']")
	WebElement supportSectionXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Getting started']")
	WebElement gettingStartedSectionXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Place an order']")
	WebElement placeanOrdertextXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Add Subscription?']")
	WebElement addSubscriptionButtonXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Buffalo Milk 500 ML']")
	WebElement buffaloMilkXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Desi Danedar Ghee 1L']")
	WebElement desiGheeXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Brown Bread 400 GM']")
	WebElement brownBreadXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Protein White Eggs 10pcs']")
	WebElement proteinWhiteEggsXpath;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	WebElement leftArrowIconXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='1']")
	WebElement qtyXpath;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/"
			+ "android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[3]")
	WebElement updateQtyXpath;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView"
			+ "/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
	WebElement deleteQtyXpath;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView"
			+ "/android.view.ViewGroup/android.view.ViewGroup[3]")
	WebElement endAtXpath;

	@FindBy(xpath = "//android.widget.Button[@text='OK']")
	WebElement okXpath;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Next month\"]")
	WebElement rightArrowIconXpath;

	@FindBy(xpath = "//android.view.View[@content-desc=\"30 June 2020\"]")
	WebElement juneParticularDateXpath;

	@FindBy(xpath = "//android.view.View[@content-desc=\"01 June 2020\"]")
	WebElement juneParticularDateDesiXpath;

	@FindBy(xpath = "//android.widget.TextView[@text='Pay ₹1500']")
	WebElement payXpath;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]")
	WebElement deliveryDateXpath;
	
	@FindBy(xpath="//android.widget.TextView[@text='+']")
	WebElement plusXpath;
	
	
	
	
	

	// Initializing the page objects
	public MyPlanPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions

	@Step("select randomly Myplan product horizontal list")
	public MyPlanPage randomSelectHorizontalProductList() throws Exception {

		
		Thread.sleep(4000);
		// click on my plan icon
		myPlanXpath.click();
		Thread.sleep(4000);

		// select random category list
//		List<MobileElement> elements =driver.findElements
//				(By.xpath("//*[@class ='android.support.v7.app.ActionBar$Tab']/android.widget.TextView"));

		// select random category list
		List<MobileElement> elements = driver
				.findElements(By.xpath("//*[@class ='android.widget.LinearLayout']/android.widget.TextView"));

		System.out.println("list count:" + elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size()-2);

		System.out.println("random no: " + rndInt);

		// select randomly any category list with any index
		elements.get(rndInt).click();

		return new MyPlanPage();

	}

	@Step("select randomly Myplan product horizontal list")
	public void fetchingProductListNameandPrices() throws Exception {

		Thread.sleep(4000);
		// click on my plan icon
		myPlanXpath.click();
		Thread.sleep(4000);

		// select random category list
		List<MobileElement> elements = driver
				.findElements(By.xpath("//*[@class ='android.widget.LinearLayout']/android.widget.TextView"));

		System.out.println("list count:" + elements.size());

		for (int i = 0; i < elements.size(); i++) {

			// click on horizontal text by one by one
			elements.get(i).click();
			
			System.out.println("Index of i is :-" + Integer.toString(i));
			

			// product wise section xpath
			String titleXpath="//androidx.recyclerview.widget.RecyclerView[@index='1']/android.widget.FrameLayout[@index=" + i + "]/android.view.ViewGroup[@index='0']/android.widget.TextView[1]";

			List<MobileElement> elementlist = driver.findElements(By.xpath(titleXpath));
			
			System.out.println("productcount: " +elementlist.size());

		
			for (int j = 0; j <=elementlist.size(); j++) {
				
				System.out.println("Index of j is :-" + Integer.toString(j));

				// xpath for title name
				String nameTextXpath = "//androidx.recyclerview.widget.RecyclerView[@index='1']/android.widget.FrameLayout[@index=" + j + "]/android.view.ViewGroup[@index='0']/android.widget.TextView[@index='1']";

				// xpath for MRP
				String mrpXpath = "//androidx.recyclerview.widget.RecyclerView[@index='1']/android.widget.FrameLayout[@index=" + j + "]/android.view.ViewGroup[@index='0']/android.widget.TextView[@index='2']";

				WebElement nameText = driver.findElement(By.xpath(nameTextXpath));

				WebElement mrpText = driver.findElement(By.xpath(mrpXpath));

				// printing product Name
				System.out.println("Product Name: " + nameText.getText().toString());

				// printing product Mrp
				System.out.println("Product MRP: " + mrpText.getText().toString());

			}
			
			System.out.println("=========================================");
		
	
			
		}
		

	}
	
	@Step("click on Buffalo milk section")
	public void orderPlaceMilkProducts() throws Exception {

	
	/*	
		Thread.sleep(4000);
		// click on my plan icon
		myPlanXpath.click();
		Thread.sleep(4000);
*/

		
		
		// select random category list
		List<MobileElement> elements = driver
				.findElements(By.xpath("//*[@class='androidx.recyclerview.widget.RecyclerView']"));

		
		System.out.println("list count:" + elements.size());

		Random rnd = new Random();
		int pi = rnd.nextInt(elements.size());
	
		System.out.println("pitext:"+pi);
		
		String particularProductSectionXpath="//androidx.recyclerview.widget.RecyclerView[@index='1']/android.widget.FrameLayout[@index=" + pi + "]";
		
		WebElement clickonParticularSectionForMilkProduct=driver.findElement(By.xpath(particularProductSectionXpath));
		
		clickonParticularSectionForMilkProduct.click();
		
	    int minimumAddition = Integer.parseInt(prop.getProperty("minimumAddition"));
		
		
		
		int i=0;

	
		while(i< minimumAddition ) {
			
			addPlusXpath.click();
			
			i++;
			
	//	    confirmXpath.click();
		
			
			
		  }
			

	}

	
	
	@Step("click on Buffalo milk section")
	public void buffaloMilkSections() throws Exception {

		// Click on buffalo milk link
		buffaloMilkXpath.click();

	}

	@Step("click on Desi Danedar GHee section")
	public void desiDanedarGheeSection() throws Exception {

		// Click on Desi Danedar GHee section
		desiGheeXpath.click();

	}

	@Step("click on  protein white egg section")
	public void proteinWhitEggSection() throws Exception {

		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator(
				"new UiScrollable(" + "new UiSelector().scrollable(true)).scrollIntoView("
						+ "new UiSelector().textContains(\"Protein White Eggs 10pcs\"));");

		// Click on protein white egg section
		proteinWhiteEggsXpath.click();

	}

	@Step("click on brown bread section")
	public void brownBreadSection() throws Exception {

		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator(
				"new UiScrollable(" + "new UiSelector().scrollable(true)).scrollIntoView("
						+ "new UiSelector().textContains(\"Brown Bread 400 GM\"));");

		// Click on brown bread section
		brownBreadXpath.click();

	}

	@Step("click on end at section")
	public void endAtSection() throws Exception {

		// Click on end at section
		endAtXpath.click();

	}

	@Step("click on end at section")
	public void deliverDateSection() throws Exception {

		// Click on DeliveryDate section
		deliveryDateXpath.click();

	}

	@Step("click on ok button")
	public void clickOnOkButton() throws Exception {

		// Click on ok button
		okXpath.click();

	}

	@Step("select a particular date")
	public void selectParticularDate() throws Exception {

		// select particular date

		rightArrowIconXpath.click();

		juneParticularDateXpath.click();

		okXpath.click();

	}

	@Step("select a particular date")
	public void selectParticularDateForDesiGhee() throws Exception {

		// select particular date

		rightArrowIconXpath.click();

		juneParticularDateDesiXpath.click();

		okXpath.click();

	}

	@Step("click on left arrow icon in edit plan page")
	public void leftArrowIcon() throws Exception {

		// Click on left arrow icon in edit plan page
		leftArrowIconXpath.click();

	}

	@Step("click on Add button")
	public void addButton() throws Exception {

		// Click on add button
		addPlusXpath.click();

	}

	@Step("click on plus icon button")
	public void updateQty() throws Exception {

		// Click on plus icon button
		updateQtyXpath.click();

	}

	@Step("click on multiple times on plus icon button")
	public void updateMultipleTimes() throws Exception {

		// Click on plus icon button
		for (int i = 0; i < 5; i++) {

			updateQtyXpath.click();

		}

	}

	@Step("click on multiple times on minus icon button")
	public void deleteMultipleTimes() throws Exception {

		// Click on minus icon button
		for (int i = 0; i < 5; i++) {

			deleteQtyXpath.click();

		}

	}

	@Step("click on minus icon button")
	public void deleteQty() throws Exception {

		// Click on minus icon button
		deleteQtyXpath.click();

	}

	@Step("click on menu icon")
	public void clickOnMenuIcon() throws Exception {

		// Click on menu icon
		menuIconXpath.click();

	}

	@Step("click on confirm button")
	public void confirmButton() throws Exception {

		// Click on add button
		confirmXpath.click();

	}

	@Step("click on myplan icon text")
	public void myplanIcon() throws Exception {

		// click on myplan icon
		myPlanXpath.click();

	}

	@Step("click on Banner My plan section")
	public void clickOnMyPlanBanner() throws Exception {

		// click on My paln Banner
		bannerMyPlanXpath.click();

	}

	@Step("click on Banner OrderNow section")
	public void clickOnOrderNowBanner() throws Exception {

		WebElement panel = driver
				.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/"
						+ "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.view.ViewGroup"));

		Dimension dim = panel.getSize();

		int height = dim.getHeight();
		int width = dim.getWidth();

		int startx = width / 2;
		int endx = width / 2;

		int starty = (int) (height * .70);
		int endy = (int) (height * .10);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, endy)).release().perform();

		// click on order now Banner
		// bannerOrderNowXpath.click();

	}

	@Step("click on My plan Icon")
	public void clickOnMyPlanIcon() throws Exception {

		// click on MyPlan Icon
		myPlanXpath.click();

	}

	@Step("Move to Support Section click on Add Subscrition BUtton")
	public void clickOnAddSubscriptionButton() throws Exception {

		// click on add Subscription Button
		addSubscriptionButtonXpath.click();

	}

	@Step(" click on Support Section")
	public void clickOnSupportButton() throws Exception {

		MobileElement el = (MobileElement) driver.findElementByAndroidUIAutomator(
				"new UiScrollable(" + "new UiSelector().scrollable(true)).scrollIntoView("
						+ "new UiSelector().textContains(\"Support\"));");

		// click on support Button
		supportSectionXpath.click();

	}

	@Step(" click on getting started Section")
	public void clickOnGettingStartedSection() throws Exception {

		// click on getting started section
		gettingStartedSectionXpath.click();

	}

	@Step(" click on Place an order Section")
	public void clickOnPlaceAnOrder() throws Exception {

		// click on place an order
		placeanOrdertextXpath.click();

	}

	/**
	 * Actual qty=1 for buffalo milk
	 */

	public String actualQty() {
		return qtyXpath.getText();

	}

	/**
	 * expected qty=1 for buffalo milk
	 */

	public String expectedQty() {
		return qtyXpath.getText();

	}

	/**
	 * expected pay amount=1500 for desi ghee
	 */

	public String expectedPayAmount() {
		return payXpath.getText();

	}

	@Step("Click on Buffallo Milk Increment Plus Icon")
	public void updatingProductQty() throws Exception {

		try {

			List<MobileElement> elements = driver.findElements(
					By.xpath("//android.support.v7.widget.RecyclerView[@index='1']/android.view.ViewGroup"));
			System.out.println(elements.size());

			int maxAddition = Integer.parseInt(prop.getProperty("maximumAddition"));
			int maxsingleProductAddition = Integer.parseInt(prop.getProperty("maxsingleproductclick"));

			int click_sum = 0;
			Random rnd = new Random();
			int pi = rnd.nextInt(elements.size());

			System.out.println("pitext:" + pi);

			List<MobileElement> elements_clicked = new ArrayList<MobileElement>();
			while (pi <= elements.size()) {
				if (click_sum < maxAddition) {

					System.out.println("inside click" + click_sum);

					driver.findElement(By
							.xpath("//android.view.ViewGroup[@index=" + pi + "]/android.widget.ImageView[@index='0']"))
							.click();
					click_sum = click_sum + 1;

					int rndInt = rnd.nextInt(maxsingleProductAddition - 1);
					System.out.println("Random generated" + rndInt);
					for (int j = 0; j < rndInt; j++) {
						System.out.println("INside J");
						driver.findElement(By.xpath("//android.view.ViewGroup[@index=" + pi
								+ "]/android.widget.LinearLayout[@index='2']/android.widget.ImageButton[@index='2']"))
								.click();
						click_sum = click_sum + 1;
					}
					// elements_clicked.add(elements.get(pi));
					System.out.println("Click sum=" + click_sum);
				} else {
					break;
				}
				int low = pi;
				int high = elements.size();
				pi = rnd.nextInt(high - low) + low;
				System.out.println("pitext:" + pi);

			}

		} catch (Exception e) {

			System.out.println(e.getMessage());

		}

	}

	@Step("Swiping downward")
	public void swipScreen() {

		Dimension dim = driver.manage().window().getSize();

		int height = dim.getHeight();
		int width = dim.getWidth();

		int startx = width / 2;
		int endx = width / 2;

		int starty = (int) (height * .65);
		int endy = (int) (height * .10);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, endy)).release().perform();

	}

}
