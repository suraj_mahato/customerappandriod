package com.customer_app.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class TestBase {


	public static ThreadLocal<AndroidDriver> tdriver = new ThreadLocal<AndroidDriver>();

	public static AndroidDriver driver;
	public static Properties prop;
	public static Logger log=Logger.getLogger(TestBase.class);
	
	public static Properties Config=new Properties();
	public static FileInputStream fis;

	public TestBase() {
		
	
		try {

			prop = new Properties();
		
			FileInputStream ip = new FileInputStream(
					"/home/dell/eclipse-workspace/customer-app-automation/src/main/resources/properties/config.properties");
						
			prop.load(ip);	
			
		} catch (FileNotFoundException e) {
			System.out.println("File Not found");
			e.printStackTrace();

		} catch (IOException e) {
			System.out.println("Io Exception");
			e.printStackTrace();

		}
	}


		
	public AndroidDriver initiallization() throws MalformedURLException {
		
		DOMConfigurator.configure(prop.getProperty("logUrl"));
		
		File app_lac = new File(prop.getProperty("appPath"));
		log.info("Campaign App Location Path");
		
    	File app = new File(app_lac, prop.getProperty("application"));
		log.info("App Name");
	
        
		log.info("*******INTILIZATING CAPABILITIES*********");
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("device", prop.getProperty("deviceA"));
		log.info("DeviceA: Andriord");
		caps.setCapability("automationName", prop.getProperty("automationName1"));
		log.info("Automation Name: Appium");
		caps.setCapability("deviceName", prop.getProperty("deviceNameR"));
		log.info("Device NameN: Nokia 2_1");
		caps.setCapability("udid", prop.getProperty("udidR"));
		log.info("UDIDN: E2MID18102110362");
		caps.setCapability("platformName", prop.getProperty("platformNameA"));
		log.info("PlatformNameA: Android");
		caps.setCapability("app", app.getAbsolutePath());
		caps.setCapability("autoGrantPermission", true);
		caps.setCapability("skipDeviceInitialization", true);
		caps.setCapability("skipServerInstallation", true);
		caps.setCapability("noReset", true);
	
		
		caps.setCapability("appPackage", prop.getProperty("appPackage"));
		log.info("appPackageCampaign: campaign.countrydelight.in.campaignapp");
		caps.setCapability("appWaitActivity", prop.getProperty("appWaitActivity"));
		log.info("appWaitActivityCampaign: com.cd.campaign.SplashScreenActivity");

		driver = new AndroidDriver(new URL(prop.getProperty("appiumServer")), caps);
		log.info("*******INITIAtING ANDRIOD DRIVER************");
		log.info(" HOST: 0.0.0.0");
		log.info(" PORT: 4723");
		log.info(" URL: http://127.0.0.1:4723/wd/hub");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		log.info("Waiting to load the URL ");

		tdriver.set(driver);
		log.debug("call the driver");
		
		return getDriver();

	}

	public static synchronized AndroidDriver getDriver() {
		return tdriver.get();
	}

}

	

	

